class MockTJSDocument {
  public document: any
  set(document) {
    this.document = document
  }
  get() {
    return this.document
  }
}

class MockSvelteApplication {
  close() {
    return
  }

  reactive = {}

  _getHeaderButtons() {
    return []
  }
}

class MockItemSheetShellSvelte {}

class MockSidEditor {
  public item: any
  constructor(item) {
    this.item = item
  }
  render() {
    return
  }
}

vi.mock('@typhonjs-fvtt/runtime/svelte/store/fvtt/document', () => ({
  TJSDocument: MockTJSDocument,
}))
vi.mock('@typhonjs-fvtt/runtime/svelte/application', () => ({
  SvelteApplication: MockSvelteApplication,
}))
vi.mock('../../src/ui/item-sheet/ItemSheetShell.svelte', () => ({
  default: MockItemSheetShellSvelte,
}))
vi.mock('../../src/module/item/sid-editor-sheet.js', () => ({
  SidEditorSheet: MockSidEditor,
}))

global.foundry = {
  utils: {
    mergeObject: (org, other) => other,
  },
}

import { getGame } from '../../src/module/utils.js'
getGame.mockReturnValue({ user: { isGM: true }, i18n: { localize: (a) => a } })

const { SvelteItemSheet } = await import(
  '../../src/module/item/item-sheet-svelte.js'
)

describe('SvelteItemSheet', () => {
  it('to return wanted default options', () => {
    const options = SvelteItemSheet.defaultOptions

    expect(options).toMatchObject<any>({
      width: 950,
      resizable: true,
      minimizable: true,
      svelte: expect.objectContaining({
        class: MockItemSheetShellSvelte,
        target: document.body,
      }),
    })
  })

  it('to provide svelte store as prop and reactive attribute', () => {
    const doc = { test: 'test' }
    const sheet = new SvelteItemSheet(doc)
    const options = SvelteItemSheet.defaultOptions
    expect(options.svelte.props.bind(sheet)().doc.document).toEqual(doc)
    expect(sheet.reactive.document).toEqual(doc)
  })

  it('to set sid editor as header button', () => {
    const item = {
      system: {
        sid: 'test',
      },
    }
    const sheet = new SvelteItemSheet(item)
    const buttons = sheet._getHeaderButtons()
    expect(buttons[0].label).toEqual('DSA.tweaks')

    const event = { preventDefault: vi.fn() }
    buttons[0].onclick(event)
    expect(event.preventDefault).toHaveBeenCalled()
  })
})
