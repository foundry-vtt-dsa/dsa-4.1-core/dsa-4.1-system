import { render } from '@testing-library/svelte'
import { BaseCharacter } from '../../../src/module/model/character.js'
import Fragment from '@playpilot/svelte-fragment-component'
import html from '@playpilot/svelte-htm'
import Rest from '../../../src/ui/roll-dialog/RestRoll.svelte'
import { vi } from 'vitest'
import { fireEvent } from '@testing-library/dom'
import userEvent from '@testing-library/user-event'

vi.mock(
  '../../../src/ui/roll-dialog/Roll.svelte',
  async () => await import('./MockRoll.svelte')
)

describe('RestRoll', () => {
  const localize = (a) => a

  let promiseResolve, closePromise, context

  beforeEach(() => {
    closePromise = new Promise((resolve) => {
      promiseResolve = resolve
    })
    context = {
      '#managedPromise': { get: () => closePromise },
    }
  })

  test('Initial rendering with only vitality and submitting', async () => {
    const vitalityObject = {
      modifier: 0,
      roll: true,
    }

    const character = {
      determineResting: vi.fn().mockReturnValue({
        vitality: vitalityObject,
      }),
      rest: vi.fn(),
    } as unknown as BaseCharacter

    const { getByTestId, queryByTestId } = render(
      html`
        <${Fragment} context=${context}>
          <${Rest} localize=${localize} character=${character} />
        </$>
      `
    )

    const weatherInput = getByTestId('weather-input')
    expect(weatherInput).not.toHaveValue(0)
    const restInterruptedCheckbox = getByTestId('restInterrupted-checkbox')
    expect(restInterruptedCheckbox).not.toBeChecked()
    const keptWatchCheckbox = getByTestId('keptWatch-checkbox')
    expect(keptWatchCheckbox).not.toBeChecked()
    const badEncampmentCheckbox = getByTestId('badEncampment-checkbox')
    expect(badEncampmentCheckbox).not.toBeChecked()
    const vitalityText = getByTestId('vitality-mod')
    expect(vitalityText).toHaveTextContent('1d6')
    const vitalityInput = getByTestId('vitalityMod-input')
    expect(vitalityInput).toHaveValue(0)
    const astralEnergyText = queryByTestId('astralEnergy-mod')
    expect(astralEnergyText).not.toBeInTheDocument()
    const astralEnergyInput = queryByTestId('astralEnergyMod-input')
    expect(astralEnergyInput).not.toBeInTheDocument()

    promiseResolve(true)

    await closePromise

    expect(character.determineResting).toHaveBeenCalledWith({
      comfort: 0,
      weather: 0,
      restInterrupted: false,
      keptWatch: false,
      badEncampment: false,
      vitalityMod: 0,
      astralEnergyMod: 0,
    })

    expect(character.rest).toHaveBeenCalledWith({
      vitality: vitalityObject,
    })
  })

  test('Initial rendering with astralEnergy and submitting', async () => {
    const vitalityObject = {
      modifier: 0,
      roll: true,
    }
    const astralEnergyObject = {
      modifier: 2,
      roll: true,
    }

    const character = {
      determineResting: vi.fn().mockReturnValue({
        vitality: vitalityObject,
        astralEnergy: astralEnergyObject,
      }),
      rest: vi.fn(),
    } as unknown as BaseCharacter

    const { getByTestId } = render(
      html`
        <${Fragment} context=${context}>
          <${Rest} localize=${localize} character=${character} />
        </$>
      `
    )

    const weatherInput = getByTestId('weather-input')
    expect(weatherInput).not.toHaveValue(0)
    const restInterruptedCheckbox = getByTestId('restInterrupted-checkbox')
    expect(restInterruptedCheckbox).not.toBeChecked()
    const keptWatchCheckbox = getByTestId('keptWatch-checkbox')
    expect(keptWatchCheckbox).not.toBeChecked()
    const badEncampmentCheckbox = getByTestId('badEncampment-checkbox')
    expect(badEncampmentCheckbox).not.toBeChecked()
    const vitalityText = getByTestId('vitality-mod')
    expect(vitalityText).toHaveTextContent('1d6')
    const vitalityInput = getByTestId('vitalityMod-input')
    expect(vitalityInput).toHaveValue(0)
    const astralEnergyText = getByTestId('astralEnergy-mod')
    expect(astralEnergyText).toHaveTextContent('1d6 +2')
    const astralEnergyInput = getByTestId('astralEnergyMod-input')
    expect(astralEnergyInput).toHaveValue(0)

    promiseResolve(true)

    await closePromise

    expect(character.determineResting).toHaveBeenCalledWith({
      comfort: 0,
      weather: 0,
      restInterrupted: false,
      keptWatch: false,
      badEncampment: false,
      vitalityMod: 0,
      astralEnergyMod: 0,
    })

    expect(character.rest).toHaveBeenCalledWith({
      vitality: vitalityObject,
      astralEnergy: astralEnergyObject,
    })
  })

  test.each([
    {
      modifier: 0,
      roll: true,
      expected: '1d6',
    },
    {
      modifier: -3,
      roll: true,
      expected: '1d6 -3',
    },
    {
      modifier: 2,
      roll: true,
      expected: '1d6 +2',
    },
    {
      modifier: -2,
      roll: false,
      expected: '-2',
    },
    {
      modifier: 6,
      roll: false,
      expected: '+6',
    },
  ])('Renders the modifier values correct', (testParameters) => {
    const character = {
      determineResting: vi.fn().mockReturnValue({
        vitality: testParameters,
        astralEnergy: testParameters,
      }),
      rest: vi.fn(),
    } as unknown as BaseCharacter

    const { getByTestId } = render(
      html`
          <${Fragment} context=${context}>
            <${Rest} localize=${localize} character=${character} />
          </$>
        `
    )

    const vitalityText = getByTestId('vitality-mod')
    expect(vitalityText).toHaveTextContent(testParameters.expected)

    const astralEnergyText = getByTestId('astralEnergy-mod')
    expect(astralEnergyText).toHaveTextContent(testParameters.expected)
  })

  test.each(['restInterrupted', 'keptWatch', 'badEncampment'])(
    'Switching a flag and submitting',
    async (flag) => {
      const vitalityObject = {
        modifier: 0,
        roll: true,
      }
      const astralEnergyObject = {
        modifier: 2,
        roll: true,
      }

      const character = {
        determineResting: vi.fn().mockReturnValue({
          vitality: vitalityObject,
          astralEnergy: astralEnergyObject,
        }),
        rest: vi.fn(),
      } as unknown as BaseCharacter

      const { getByTestId } = render(
        html`
          <${Fragment} context=${context}>
            <${Rest} localize=${localize} character=${character} />
          </$>
        `
      )

      const checkbox = getByTestId(`${flag}-checkbox`)
      await checkbox.click()

      promiseResolve(true)

      await closePromise

      expect(character.determineResting).toHaveBeenCalledWith({
        comfort: 0,
        weather: 0,
        restInterrupted: flag === 'restInterrupted',
        keptWatch: flag === 'keptWatch',
        badEncampment: flag === 'badEncampment',
        vitalityMod: 0,
        astralEnergyMod: 0,
      })
    }
  )

  test('setting the weather slider and submitting', async () => {
    const vitalityObject = {
      modifier: 0,
      roll: true,
    }
    const astralEnergyObject = {
      modifier: 2,
      roll: true,
    }

    const character = {
      determineResting: vi.fn().mockReturnValue({
        vitality: vitalityObject,
        astralEnergy: astralEnergyObject,
      }),
      rest: vi.fn(),
    } as unknown as BaseCharacter

    const { getByTestId } = render(
      html`
          <${Fragment} context=${context}>
            <${Rest} localize=${localize} character=${character} />
          </$>
        `
    )

    const weatherSlider = getByTestId('weather-input')
    fireEvent.change(weatherSlider, { target: { value: -2 } })

    promiseResolve(true)

    await closePromise

    expect(character.determineResting).toHaveBeenCalledWith({
      comfort: 0,
      weather: -2,
      restInterrupted: false,
      keptWatch: false,
      badEncampment: false,
      vitalityMod: 0,
      astralEnergyMod: 0,
    })
  })

  test.each(['vitalityMod', 'astralEnergyMod'])(
    'setting the modifier inputs and submitting',
    async (property) => {
      const vitalityObject = {
        modifier: 0,
        roll: true,
      }
      const astralEnergyObject = {
        modifier: 2,
        roll: true,
      }

      const character = {
        determineResting: vi.fn().mockReturnValue({
          vitality: vitalityObject,
          astralEnergy: astralEnergyObject,
        }),
        rest: vi.fn(),
      } as unknown as BaseCharacter

      const { getByTestId } = render(
        html`
          <${Fragment} context=${context}>
            <${Rest} localize=${localize} character=${character} />
          </$>
        `
      )

      const input = getByTestId(`${property}-input`)
      await fireEvent.input(input, { target: { value: 2 } })

      promiseResolve(true)

      await closePromise

      expect(character.determineResting).toHaveBeenCalledWith({
        comfort: 0,
        weather: 0,
        restInterrupted: false,
        keptWatch: false,
        badEncampment: false,
        vitalityMod: property === 'vitalityMod' ? 2 : 0,
        astralEnergyMod: property === 'astralEnergyMod' ? 2 : 0,
      })
    }
  )

  test.each(['0', '1', '2'])(
    'selecting comfort and submitting',
    async (value) => {
      const vitalityObject = {
        modifier: 0,
        roll: true,
      }
      const astralEnergyObject = {
        modifier: 2,
        roll: true,
      }

      const character = {
        determineResting: vi.fn().mockReturnValue({
          vitality: vitalityObject,
          astralEnergy: astralEnergyObject,
        }),
        rest: vi.fn(),
      } as unknown as BaseCharacter

      const { getByTestId } = render(
        html`
        <${Fragment} context=${context}>
          <${Rest} localize=${localize} character=${character} />
        </$>
      `
      )

      const input = getByTestId('comfort-input')
      await userEvent.selectOptions(input, value)

      promiseResolve(true)

      await closePromise

      expect(character.determineResting).toHaveBeenCalledWith({
        comfort: Number.parseInt(value),
        weather: 0,
        restInterrupted: false,
        keptWatch: false,
        badEncampment: false,
        vitalityMod: 0,
        astralEnergyMod: 0,
      })
    }
  )
})
