import SkillRoll from '../../../src/ui/roll-dialog/SkillRoll.svelte'
import { render } from '@testing-library/svelte'
import userEvent from '@testing-library/user-event'

import { BaseCharacter } from '../../../src/module/model/character.js'
import { BaseSkill, Testable } from '../../../src/module/model/properties.js'
import { writable } from 'svelte/store'
import Fragment from '@playpilot/svelte-fragment-component'
import html from '@playpilot/svelte-htm'

vi.mock(
  '../../../src/ui/roll-dialog/Roll.svelte',
  async () => await import('./MockRoll.svelte')
)

describe('SkillRollDialog', () => {
  const callback = vi.fn()
  const localize = (a) => a
  const mockRoll = { submit: () => undefined }
  const context = {
    mock: writable(mockRoll),
  }
  const character = {} as BaseCharacter
  const testAttributes = ['courage', 'courage', 'courage']
  const skill = {
    testAttributes,
    value: 5,
  } as Testable<BaseSkill>

  beforeEach(() => {
    callback.mockReset()
    context.mock.set(mockRoll)
  })

  test('Submit without changes', async () => {
    render(
      html`
    <${Fragment} context=${context}>
    <${SkillRoll} callback=${callback} localize=${localize} character=${character} skill=${skill} />
    </$>
    `
    )

    mockRoll.submit()
    expect(callback).toHaveBeenCalledWith(new Map(), { testAttributes })
  })

  test('Submit with other test attributes', async () => {
    const { getByTestId } = render(
      html`
        <${Fragment} context=${context}>
        <${SkillRoll} callback=${callback} localize=${localize} character=${character} skill=${skill} />
        </$>
        `
    )

    const skillName = getByTestId('skill-name')
    expect(skillName).toHaveTextContent(skill.name + ' (' + skill.value + ')')

    const selectFirstTestAttribute = getByTestId('test-attribute-0')
    expect(selectFirstTestAttribute).toHaveValue('courage')
    expect(selectFirstTestAttribute).toHaveTextContent('courage')
    await userEvent.selectOptions(selectFirstTestAttribute, 'strength')
    expect(selectFirstTestAttribute).toHaveValue('strength')
    expect(selectFirstTestAttribute).toHaveTextContent('strength')

    const selectSecondTestAttribute = getByTestId('test-attribute-1')
    expect(selectSecondTestAttribute).toHaveValue('courage')
    expect(selectSecondTestAttribute).toHaveTextContent('courage')
    await userEvent.selectOptions(selectSecondTestAttribute, 'agility')
    expect(selectSecondTestAttribute).toHaveValue('agility')
    expect(selectSecondTestAttribute).toHaveTextContent('agility')

    const selectThirdTestAttribute = getByTestId('test-attribute-2')
    expect(selectThirdTestAttribute).toHaveValue('courage')
    expect(selectThirdTestAttribute).toHaveTextContent('courage')
    await userEvent.selectOptions(selectThirdTestAttribute, 'intuition')
    expect(selectThirdTestAttribute).toHaveValue('intuition')
    expect(selectThirdTestAttribute).toHaveTextContent('intuition')

    mockRoll.submit()

    const expectedTestAttributes = ['strength', 'agility', 'intuition']
    expect(callback).toHaveBeenCalledWith(new Map(), {
      testAttributes: expectedTestAttributes,
    })
  })
})
