import { render } from '@testing-library/svelte'

import html from '@playpilot/svelte-htm'
import Fragment from '@playpilot/svelte-fragment-component'
import { writable } from 'svelte/store'

import Input from '../../src/ui/Input.svelte'

describe('Input', () => {
  test('Is rendered correctly in edit mode', async () => {
    const value = 5
    const context = {
      isEditMode: writable(true),
    }
    const { getByRole } = render(
      html`
        <${Fragment} context=${context}>
          <${Input} value=${value}/>
        </$>
        `
    )

    const input = getByRole('spinbutton') as HTMLInputElement
    expect(input).toBeInTheDocument()
    expect(input).toHaveValue(value)
    expect(input.type).toBe('number')
  })

  test('Is rendered correctly in edit mode for text value', async () => {
    const value = 'test'
    const context = {
      isEditMode: writable(true),
    }
    const { getByRole } = render(
      html`
        <${Fragment} context=${context}>
          <${Input} value=${value} type="text"/>
        </$>
        `
    )

    const input = getByRole('textbox') as HTMLInputElement
    expect(input).toBeInTheDocument()
    expect(input).toHaveValue(value)
    expect(input.type).toBe('text')
  })

  test('Is rendered correctly without edit mode with value', async () => {
    const value = 5
    const context = {
      isEditMode: writable(false),
    }
    const { getByText } = render(
      html`
        <${Fragment} context=${context}>
          <${Input} value=${value}/>
        </$>
        `
    )

    const text = getByText(value.toString())
    expect(text).toBeInTheDocument()
  })

  test('Is rendered correctly without edit mode and without value', async () => {
    const value = null
    const context = {
      isEditMode: writable(false),
    }
    const { getByText } = render(
      html`
        <${Fragment} context=${context}>
          <${Input} value=${value}/>
        </$>
        `
    )

    const text = getByText('-')
    expect(text).toBeInTheDocument()
  })
})
