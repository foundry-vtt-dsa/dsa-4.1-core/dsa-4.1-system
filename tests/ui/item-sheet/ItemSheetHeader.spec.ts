import { render } from '@testing-library/svelte'

import html from '@playpilot/svelte-htm'
import Fragment from '@playpilot/svelte-fragment-component'

vi.mock(
  '../../../src/ui/Dialog.svelte',
  async () => await import('../MockDialog.svelte')
)

import { getGame } from '../../../src/module/utils.js'
;(getGame as Mock<any>).mockImplementation(() => ({
  ruleset: createMockRuleset(),
}))

import { Mock } from 'vitest'
import { createMockRuleset } from '../../ruleset/rules/helpers.js'
import { writable } from 'svelte/store'
import ItemSheetHeader from '../../../src/ui/item-sheet/ItemSheetHeader.svelte'
import { fireEvent } from '@testing-library/dom'

describe('ItemSheetHeader', () => {
  test('Is rendered correctly and can update the value', async () => {
    const doc = {
      name: 'testName',
      system: {},
    }
    const context = {
      doc: writable(doc),
      '#external': {
        application: {},
      },
    }
    const { getByTestId } = render(
      html`
      <${Fragment} context=${context}>
      <${ItemSheetHeader} />
      </$>
      `
    )

    const itemName = getByTestId('item-name').querySelector('input')
    expect(itemName?.value).toBe('testName')

    await fireEvent.input(itemName, { target: { value: 'newName' } })
    expect(itemName?.value).toBe('newName')
  })
})
