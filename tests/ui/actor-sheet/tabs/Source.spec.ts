import { fireEvent, render } from '@testing-library/svelte'

import html from '@playpilot/svelte-htm'
import Fragment from '@playpilot/svelte-fragment-component'

vi.mock(
  '../../../../src/ui/Dialog.svelte',
  async () => await import('../../MockDialog.svelte')
)
import { getGame } from '../../../../src/module/utils.js'

import Source from '../../../../src/ui/actor-sheet/tabs/Source.svelte'
import { writable } from 'svelte/store'
import { Mock } from 'vitest'
import { createMockRuleset } from '../../../ruleset/rules/helpers.js'

describe('Source', () => {
  test('Is rendered correctly', async () => {
    const ruleset = createMockRuleset()

    ;(getGame as Mock<any>).mockImplementation(() => ({
      ruleset,
    }))

    const sourceName = 'Carafei'
    const value = 11
    const sphere = 'demonic'

    const source = {
      name: sourceName,
      system: {
        value,
        sphere,
      },
      type: 'source',
      sheet: {
        render: vi.fn(),
      },
      update: vi.fn(),
    }
    const context = {
      '#external': {},
      isEditMode: writable(true),
      doc: writable({
        system: {
          base: {},
        },
        talent: () => ({}),
        talents: [],
      }),

      openDialogMock: vi.fn(),
    }
    const { getByTestId } = render(
      html`
      <${Fragment} context=${context}>
      <${Source} source=${source} />
      </$>
      `
    )

    const sourceRoll = getByTestId('source-roll')
    const sourceEdit = getByTestId('source-edit')
    const sourceValue = getByTestId('source-value')

    expect(sourceEdit).toBeInTheDocument()
    expect(sourceRoll).toHaveTextContent(
      `${sourceName} ( courage_abbr / courage_abbr / charisma_abbr )`
    )
    console.log(sourceValue)
    expect(sourceValue.children[0]).toHaveValue(value)

    await fireEvent.click(sourceEdit)
    expect(source.sheet.render).toHaveBeenCalled()

    await fireEvent.click(sourceRoll)
    expect(context.openDialogMock).toHaveBeenCalled()
    expect(ruleset.execute).toHaveBeenCalled()
  })

  test('Is rendered correctly in view mode', async () => {
    const ruleset = createMockRuleset()

    ;(getGame as Mock<any>).mockImplementation(() => ({
      ruleset,
    }))

    const sourceName = 'Carafei'
    const value = '11'
    const sphere = 'demonic'

    const source = {
      name: sourceName,
      system: {
        value,
        sphere,
      },
      type: 'source',
      sheet: {
        render: vi.fn(),
      },
      update: vi.fn(),
    }
    const context = {
      '#external': {},
      isEditMode: writable(true),
      doc: writable({
        system: {
          base: {},
        },
        talent: () => ({}),
        talents: [],
      }),

      openDialogMock: vi.fn(),
    }
    const { getByTestId } = render(
      html`
      <${Fragment} context=${context}>
      <${Source} source=${source} />
      </$>
      `
    )

    const sourceRoll = getByTestId('source-roll')
    const sourceEdit = getByTestId('source-edit')
    const sourceValue = getByTestId('source-value')

    expect(sourceEdit).toBeInTheDocument()
    expect(sourceRoll).toHaveTextContent(
      `${sourceName} ( courage_abbr / courage_abbr / charisma_abbr )`
    )
    expect(sourceValue.children[0].value).toBe(value)
    await fireEvent.click(sourceRoll)

    expect(context.openDialogMock).toHaveBeenCalled()
    expect(ruleset.execute).toHaveBeenCalled()
  })
})
