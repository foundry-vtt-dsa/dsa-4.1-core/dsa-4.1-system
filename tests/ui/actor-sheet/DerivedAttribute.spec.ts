import { render } from '@testing-library/svelte'

import html from '@playpilot/svelte-htm'

import DerivedAttribute from '../../../src/ui/actor-sheet/DerivedAttribute.svelte'

describe('DerivedAttribute', () => {
  test('Is rendered correctly and can update the value', async () => {
    const attributeName = 'testAttribute'
    const value = 5
    const { getByTestId } = render(
      html`<${DerivedAttribute} name=${attributeName} value=${value} />`
    )

    expect(getByTestId('derived-attribute')).toHaveTextContent(attributeName)
    expect(getByTestId('derived-attribute')).toHaveTextContent(value.toString())
  })
})
