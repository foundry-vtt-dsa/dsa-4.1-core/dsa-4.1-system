import { render } from '@testing-library/svelte'
import { writable } from 'svelte/store'
import ApplyDamage from '../../../src/ui/apply-damage/ApplyDamage.svelte'
import html from '@playpilot/svelte-htm'
import Fragment from '@playpilot/svelte-fragment-component'

describe('Roll', () => {
  test.each([true, false])(
    'Is rendered correctly and submits correctly',
    async (withProvidedHitPoints: boolean) => {
      const localize = (a) => a
      const closePromise = new Promise((resolve) => resolve(true))
      const context = {
        '#managedPromise': { get: () => closePromise },
      }

      const character = {
        takeDamage: vi.fn(),
        determineDamage: vi.fn().mockReturnValue({ damage: 5, wounds: 1 }),
      }

      let hitPoints: any = undefined
      let expectedHitPoints: number = 0
      if (withProvidedHitPoints) {
        hitPoints = 5
        expectedHitPoints = 5
      }

      const { getByTestId } = render(html`
      <${Fragment} context=${context}>
        <${ApplyDamage} localize=${localize} bind:character=${writable(
          character
        )} hitPoints=${hitPoints} />
      </$>
    `)

      const hitPointInput = getByTestId('hitpoint-input')
      expect(hitPointInput).toBeInTheDocument()
      expect(hitPointInput).toHaveValue(expectedHitPoints)
      expect(hitPointInput.parentElement).toHaveTextContent('hitPoints')

      const takenDamage = getByTestId('damage-to-take')
      expect(takenDamage).toBeInTheDocument()
      expect(takenDamage).toHaveTextContent('damage: 5')

      const takenWounds = getByTestId('wounds-to-take')
      expect(takenWounds).toBeInTheDocument()
      expect(takenWounds).toHaveTextContent('wounds: 1')

      await closePromise
      expect(character.takeDamage).toHaveBeenCalledWith({
        damage: 5,
        wounds: 1,
      })
    }
  )
})
