import { Maneuver } from '../../src/module/character/maneuver.js'
import { ManeuverType } from '../../src/module/model/modifier.js'

describe('Maneuver', function () {
  const name = 'Wuchtschlag'
  const maneuverType = 'offensive'

  it('should be have a name and default values for minMod and mod', function () {
    const maneuver = new Maneuver(name, maneuverType)
    expect(maneuver.name).toEqual(name)
    expect(maneuver.minMod).toEqual(1)
    expect(maneuver.mod).toEqual(1)
  })

  test.each(['offensive', 'defensive'] as ManeuverType[])(
    'should be either of offensive or defensive type',
    function (type: ManeuverType) {
      const typedManeuver = new Maneuver(name, type)
      expect(typedManeuver.type).toEqual(type)
    }
  )

  it('should have a minimal modfier bound with default one which can be set via options', function () {
    const minMod = 6
    const maneuver = new Maneuver(name, maneuverType, { minMod })
    expect(maneuver.minMod).toEqual(minMod)
  })

  it('should have a modifier which can be modified', function () {
    const maneuver = new Maneuver(name, maneuverType)
    const mod = 6
    maneuver.mod = mod
    expect(maneuver.mod).toEqual(mod)
  })
})
