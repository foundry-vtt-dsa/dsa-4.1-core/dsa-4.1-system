import {
  ComputeAttack,
  ComputeParry,
} from '../../../src/module/ruleset/rules/derived-combat-attributes.js'
import { WeaponModifierRule } from '../../../src/module/ruleset/rules/weapon-modifier.js'
import type { CombatTalent } from '../../../src/module/model/properties.js'
import type {
  BaseCharacter,
  CharacterDataAccessor,
} from '../../../src/module/model/character.js'
import type { MeleeWeapon } from '../../../src/module/model/items.js'
import { Computation } from '../../../src/module/ruleset/rule-components.js'
import { createTestRuleset } from './helpers.js'

describe('WeaponModifiers', function () {
  let character: BaseCharacter

  beforeEach(() => {
    character = {
      data: {} as CharacterDataAccessor,
    } as BaseCharacter
  })

  const ruleset = createTestRuleset()

  const computeAttack = vi.fn()
  ruleset.registerComputation(new Computation(ComputeAttack, computeAttack))

  const computeParry = vi.fn()
  ruleset.registerComputation(new Computation(ComputeParry, computeParry))

  ruleset.add(WeaponModifierRule)
  ruleset.compileRules()

  const combatTalent = {} as CombatTalent

  const weapon = {
    type: 'melee',
  } as MeleeWeapon

  it('should add weapon modifiers to the attack value', function () {
    const baseAttack = 8
    character.data.baseAttack = baseAttack

    const attackMod = 5
    combatTalent.attackMod = attackMod
    computeAttack.mockReturnValue({ value: baseAttack + attackMod })

    const weaponAttackMod = -3
    weapon.weaponMod = {
      attack: weaponAttackMod,
      parry: 0,
    }

    const expectedAttack = 10

    const result = ruleset.compute(ComputeAttack, {
      character,
      talent: combatTalent,
      weapon,
    })

    expect(result.value).toEqual(expectedAttack)
  })

  it('should use weapon modifiers for the parry value', function () {
    const baseParry = 9
    character.data.baseParry = baseParry

    const parryMod = 5
    combatTalent.parryMod = parryMod
    computeParry.mockReturnValue({ value: baseParry + parryMod })

    const weaponParryMod = 3
    weapon.weaponMod = {
      attack: 0,
      parry: weaponParryMod,
    }

    const expectedParry = 17

    const result = ruleset.compute(ComputeParry, {
      character,
      talent: combatTalent,
      weapon,
    })

    expect(result.value).toEqual(expectedParry)
  })
})
