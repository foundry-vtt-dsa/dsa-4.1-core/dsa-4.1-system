import { when } from 'jest-when'
import type {
  BaseCharacter,
  CharacterDataAccessor,
} from '../../../src/module/model/character.js'
import {
  TestAction,
  TestEffect,
  TestEffectListenedResult,
} from '../test-classes.js'
import type {
  NamedAttribute,
  Rollable,
} from '../../../src/module/model/properties.js'
import { createTestRuleset } from './helpers.js'
import {
  BasicRestingActionRule,
  RestingAction,
  RollRestingToChatEffect,
} from '../../../src/module/ruleset/rules/basic-resting-action.js'
import {
  BasicRoll,
  RollAttributeWithoutChat,
} from '../../../src/module/ruleset/rules/basic-roll-mechanic.js'

describe('BasicRestingAction', function () {
  const ruleset = createTestRuleset()

  ruleset.registerEffect<TestEffectListenedResult, TestEffect>(
    new TestEffect(RollRestingToChatEffect)
  )

  ruleset.add(BasicRestingActionRule)
  ruleset.compileRules()

  const attributeRollMock = vi.fn()
  const intuition = {
    name: 'intuition',
    value: 10,
    roll: attributeRollMock,
  } as Rollable<NamedAttribute>

  const constitution = {
    name: 'constitution',
    value: 11,
    roll: attributeRollMock,
  } as Rollable<NamedAttribute>

  const character = {
    data: {} as CharacterDataAccessor,
  } as BaseCharacter

  character.attribute = vi.fn()
  character.applyResting = vi.fn()
  when(character.attribute)
    .calledWith(intuition.name)
    .mockReturnValue(intuition)
  when(character.attribute)
    .calledWith(constitution.name)
    .mockReturnValue(constitution)

  it('should provide basic resting action', async function () {
    const rollAttributeWithoutChatExecuteHook = vi.fn().mockReturnValue({
      success: false,
    })
    const RollAttributeWithoutChatAction = new TestAction(
      RollAttributeWithoutChat,
      rollAttributeWithoutChatExecuteHook
    )
    ruleset.registerAction(RollAttributeWithoutChatAction)

    const options = {
      vitality: {
        roll: false,
        modifier: 4,
        bonusModifier: -1,
        bonusAttribute: 'constitution',
      },
    }

    await ruleset.execute(RestingAction, {
      character,
      options,
    })

    expect(character.applyResting).toBeCalledWith(4, undefined)
  })

  test.each([
    {
      options: {
        vitality: {
          roll: true,
          bonusAttribute: 'constitution',
          modifier: 2,
          bonusModifier: -2,
        },
      },
      vitality: {
        rollResult: 4,
        expected: 7,
        bonusSuccess: true,
      },
    },
    {
      options: {
        vitality: {
          roll: true,
          bonusAttribute: 'constitution',
          modifier: 2,
          bonusModifier: -2,
        },
      },
      vitality: {
        rollResult: 4,
        expected: 6,
        bonusSuccess: false,
      },
    },
    {
      options: {
        vitality: {
          roll: true,
          bonusAttribute: 'constitution',
          modifier: 2,
          bonusModifier: -2,
        },
        astralEnergy: {
          roll: true,
          bonusAttribute: 'intuition',
          modifier: 5,
        },
      },
      vitality: {
        rollResult: 4,
        expected: 7,
        bonusSuccess: true,
      },
      astralEnergy: {
        rollResult: 6,
        expected: 12,
        bonusSuccess: true,
      },
    },
    {
      options: {
        vitality: {
          roll: true,
          bonusAttribute: 'constitution',
          modifier: 2,
          bonusModifier: -2,
        },
        astralEnergy: {
          roll: true,
          bonusAttribute: 'intuition',
          modifier: 5,
          bonusModifier: -2,
        },
      },
      vitality: {
        rollResult: 4,
        expected: 7,
        bonusSuccess: true,
      },
      astralEnergy: {
        rollResult: 6,
        expected: 11,
        bonusSuccess: false,
      },
    },
    {
      options: {
        vitality: {
          roll: true,
          bonusAttribute: 'constitution',
          modifier: 2,
          bonusModifier: -2,
        },
        astralEnergy: {
          roll: false,
          bonusAttribute: 'intuition',
          modifier: 5,
          bonusModifier: -2,
        },
      },
      vitality: {
        rollResult: 4,
        expected: 7,
        bonusSuccess: true,
      },
      astralEnergy: {
        expected: 6,
        bonusSuccess: true,
      },
    },
  ])('should apply the resting correct', async function (testParameters) {
    const basicRollExecuteHook = vi.fn().mockImplementation((options) => {
      const mod = Number.parseInt(options.formula.substring(4))
      const resourceRoll =
        options.bonusAttribute === 'constitution'
          ? testParameters.vitality.rollResult
          : testParameters.astralEnergy.rollResult
      return {
        roll: {
          total: resourceRoll + mod,
        },
      }
    })
    const basicRollAction = new TestAction(BasicRoll, basicRollExecuteHook)
    ruleset.registerAction(basicRollAction)

    const rollAttributeWithoutChatExecuteHook = vi
      .fn()
      .mockImplementation((options) => {
        return {
          success:
            options.attributeName === 'constitution'
              ? testParameters.vitality.bonusSuccess
              : testParameters.astralEnergy.bonusSuccess,
        }
      })
    const RollAttributeWithoutChatAction = new TestAction(
      RollAttributeWithoutChat,
      rollAttributeWithoutChatExecuteHook
    )
    ruleset.registerAction(RollAttributeWithoutChatAction)

    await ruleset.execute(RestingAction, {
      character,
      options: testParameters.options,
    })

    expect(character.applyResting).toBeCalledWith(
      testParameters.vitality.expected,
      testParameters.astralEnergy?.expected
    )

    const expectedVitalityBaseCall = {
      action: {
        name: 'basicRoll',
        type: 'action',
      },
      bonusAttribute: 'constitution',
      formula: `1d6+${testParameters.options.vitality.modifier}`,
    }

    expect(basicRollExecuteHook).toHaveBeenCalledWith(
      expect.objectContaining(expectedVitalityBaseCall)
    )

    if (testParameters.options.astralEnergy?.roll) {
      const expectedAstralEnergyBaseCall = {
        action: {
          name: 'basicRoll',
          type: 'action',
        },
        bonusAttribute: 'intuition',
        formula: `1d6+${testParameters.options.astralEnergy.modifier}`,
      }

      expect(basicRollExecuteHook).toHaveBeenCalledWith(
        expect.objectContaining(expectedAstralEnergyBaseCall)
      )
    } else {
      expect(basicRollExecuteHook).not.toHaveBeenCalledWith(
        expect.objectContaining({ bonusAttribute: 'intuition' })
      )
    }

    const expectedVitalityAttributeRollCall = {
      action: {
        name: 'rollAttributeWithoutChat',
        type: 'action',
      },
      attributeName: 'constitution',
      targetValue: 11,
      mod: testParameters.options.vitality.bonusModifier,
    }

    expect(rollAttributeWithoutChatExecuteHook).toBeCalledWith(
      expect.objectContaining(expectedVitalityAttributeRollCall)
    )
  })
})
