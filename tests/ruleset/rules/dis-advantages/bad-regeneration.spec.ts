import { when } from 'jest-when'

import type { BaseCharacter } from '../../../../src/module/model/character.js'
import { Computation } from '../../../../src/module/ruleset/rule-components.js'
import { createTestRuleset } from '../helpers.js'
import { ComputeVitalityRestingModifier } from '../../../../src/module/ruleset/rules/basic-resting-computation.js'
import { vi } from 'vitest'
import {
  BadRegeneration,
  BadRegenerationRule,
} from '../../../../src/module/ruleset/rules/dis-advantages/bad-regeneration.js'

let character
let ruleset

describe('Bad Regeneration rule', function () {
  it('should calculate values if the character has no Bad Regeneration', async function () {
    const options = {
      character,
    }
    when(character.has).calledWith(BadRegeneration).mockReturnValue(false)

    const result = await ruleset.compute(
      ComputeVitalityRestingModifier,
      options
    )

    expect(result).toEqual({
      modifier: 0,
      bonusModifier: 0,
      roll: true,
      bonusAttribute: 'constitution',
    })
  })

  it('should calculate values if the character has Bad Regeneration', async function () {
    const options = {
      character,
    }
    when(character.has).calledWith(BadRegeneration).mockReturnValue(true)

    const result = await ruleset.compute(
      ComputeVitalityRestingModifier,
      options
    )

    expect(result).toEqual({
      modifier: -1,
      bonusModifier: 2,
      roll: true,
      bonusAttribute: 'constitution',
    })
  })

  test.each([
    {
      level: 1,
      expected: -1,
      expectedBonus: 1,
    },
    {
      level: 2,
      expected: -2,
      expectedBonus: 2,
    },
    {
      level: 3,
      expected: -3,
      expectedBonus: 3,
    },
  ])(
    'should calculate values  if the character has Bad Regeneration (myranor version)',
    async function (testParameters) {
      const options = {
        character,
      }
      when(character.has).calledWith(BadRegeneration).mockReturnValue(true)
      when(character.data.disadvantage)
        .calledWith(BadRegeneration.identifier)
        .mockReturnValue({
          system: { value: testParameters.level },
        })

      const result = await ruleset.compute(
        ComputeVitalityRestingModifier,
        options
      )

      expect(result).toEqual({
        modifier: testParameters.expected,
        bonusModifier: testParameters.expectedBonus,
        roll: true,
        bonusAttribute: 'constitution',
      })
    }
  )

  beforeEach(() => {
    character = {
      data: {},
    } as BaseCharacter
    ruleset = createTestRuleset()

    const executeHook = vi.fn().mockReturnValue({
      modifier: 0,
      bonusModifier: 0,
      roll: true,
      bonusAttribute: 'constitution',
    })
    ruleset.registerComputation(
      new Computation(ComputeVitalityRestingModifier, executeHook)
    )

    ruleset.add(BadRegenerationRule)
    ruleset.compileRules()

    character.has = vi.fn()
    character.data.disadvantage = vi.fn()
  })
})
