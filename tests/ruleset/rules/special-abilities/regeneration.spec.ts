import { when } from 'jest-when'
import {
  RegenerationI,
  RegenerationII,
  MasterRegeneration,
  RegenerationRule,
} from '../../../../src/module/ruleset/rules/special-abilities/regeneration.js'

import type { BaseCharacter } from '../../../../src/module/model/character.js'
import { Computation } from '../../../../src/module/ruleset/rule-components.js'
import { createTestRuleset } from '../helpers.js'
import { ComputeAstralRestingModifier } from '../../../../src/module/ruleset/rules/basic-resting-computation.js'
import { vi } from 'vitest'

function mockRegenerationSpecialAbilities(character, level) {
  when(character.has)
    .calledWith(RegenerationI)
    .mockReturnValue(level >= 1)
  when(character.has)
    .calledWith(RegenerationII)
    .mockReturnValue(level >= 2)
  when(character.has)
    .calledWith(MasterRegeneration)
    .mockReturnValue(level >= 3)
}

let character
let ruleset

describe('Regeneration rule', function () {
  test.each([
    {
      level: 1,
      expected: 1,
      expectedBonus: -1,
      expectedRoll: true,
    },
    {
      level: 2,
      expected: 2,
      expectedBonus: -2,
      expectedRoll: true,
    },
    {
      intuition: 10,
      level: 3,
      expected: 6,
      expectedBonus: -3,
      expectedRoll: false,
    },
    {
      intuition: 11,
      level: 3,
      expected: 7,
      expectedBonus: -3,
      expectedRoll: false,
    },
    {
      intuition: 12,
      level: 3,
      expected: 7,
      expectedBonus: -3,
      expectedRoll: false,
    },
  ])(
    'should calculate values for Master Regeneration',
    async function (testParameters) {
      when(character.attribute)
        .calledWith('intuition')
        .mockReturnValue({ value: testParameters.intuition })

      const options = {
        character,
      }

      mockRegenerationSpecialAbilities(character, testParameters.level)

      const result = await ruleset.compute(
        ComputeAstralRestingModifier,
        options
      )

      expect(result).toEqual({
        modifier: testParameters.expected,
        bonusModifier: testParameters.expectedBonus,
        roll: testParameters.expectedRoll,
        bonusAttribute: 'intuition',
      })
    }
  )

  beforeEach(() => {
    character = {} as BaseCharacter
    ruleset = createTestRuleset()

    const executeHook = vi.fn().mockReturnValue({
      modifier: 0,
      bonusModifier: 0,
      roll: true,
      bonusAttribute: 'intuition',
    })
    ruleset.registerComputation(
      new Computation(ComputeAstralRestingModifier, executeHook)
    )

    ruleset.add(RegenerationRule)
    ruleset.compileRules()

    character.has = vi.fn()
    character.attribute = vi.fn()
  })
})
