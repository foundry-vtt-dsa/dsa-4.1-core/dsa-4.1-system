module.exports = {
  env: {
    browser: true,
    es2021: true,
  },
  extends: [
    // '@typhonjs-config/eslint-config/esm/2022/browser',
    'eslint:recommended',
    'plugin:@typescript-eslint/recommended',
    '@typhonjs-fvtt/eslint-config-foundry.js/0.8.0',
    'prettier',
    'plugin:svelte/recommended',
    'plugin:jest-dom/recommended',
  ],
  parserOptions: {
    project: './tsconfig.eslint.json',
    sourceType: 'module',
    ecmaVersion: 2020,
    extraFileExtensions: ['.svelte'],
  },
  plugins: ['jest-dom'],
  parser: '@typescript-eslint/parser',
  overrides: [
    {
      files: ['*.svelte'],
      plugins: ['jest-dom'],
      parser: 'svelte-eslint-parser',
      // Parse the `<script>` in `.svelte` as TypeScript by adding the following configuration.
      parserOptions: {
        parser: '@typescript-eslint/parser',
      },
      rules: {
        '@typescript-eslint/no-explicit-any': 'off',
        'no-unused-vars': 'off',
        '@typescript-eslint/no-unused-vars': [
          'error',
          { argsIgnorePattern: '^_' },
        ],
        // 'no-console': 'off',
      },
    },
  ],
  rules: {
    '@typescript-eslint/no-explicit-any': 'off',
    'no-unused-vars': 'off',
    '@typescript-eslint/no-unused-vars': ['error', { argsIgnorePattern: '^_' }],
    // 'no-console': 'off',
  },
  settings: {
    'svelte3/typescript': () => require('typescript'),
  },
}
