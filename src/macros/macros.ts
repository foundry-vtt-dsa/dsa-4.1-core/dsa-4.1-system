import { getGame } from '../module/utils.js'

export function registerMacros() {
  const game = getGame()
  game.dsaMacros = {}
  game.dsaMacros.addArmatrutz = addArmatrutz
}

function addArmatrutz(actor: Actor) {
  new Dialog(
    {
      title: 'Armatrutz',
      content: `<form>
         <center>
           <input class="number" type="number"></input>
         </center>
        </form>`,
      buttons: {
        roll: {
          label: `Roll`,
          callback: (html) => {
            const armor = parseInt(html.find('.number').val())
            const effectChanges = [
              {
                key: 'system.additionalItems',
                value: JSON.stringify({
                  name: 'Armatrutz',
                  type: 'armor',
                  system: {
                    equipped: true,
                    armorClass: armor,
                  },
                }),
                mode: CONST.ACTIVE_EFFECT_MODES.ADD,
              },
            ]
            const effectData = {
              name: 'Armatrutz',
              icon: 'icons/svg/blood.svg',
              changes: effectChanges,
            }
            actor.createEmbeddedDocuments('ActiveEffect', [effectData])
          },
        },
      },
      default: 'roll',
      render: (html) => {
        html.find('.number').focus()
      },
      close: (_html) => {
        return
      },
    },
    { width: 200 }
  ).render(true)
}
