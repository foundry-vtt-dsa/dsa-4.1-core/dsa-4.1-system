import type { DataAccessor } from '../model/item-data.js'
import type { Shield, WeaponMod } from '../model/items.js'

export class GenericShield implements Shield {
  public class = 'shield' as const

  private item: DataAccessor<'shield'>
  constructor(item: DataAccessor<'shield'>) {
    this.item = item
  }

  get name(): string {
    return this.item.name || ''
  }

  get initiativeMod(): number {
    return this.item.system.initiativeMod
  }

  get weaponMod(): WeaponMod {
    return {
      attack: this.item.system.weaponMod.attack,
      parry: this.item.system.weaponMod.parry,
    }
  }
}
