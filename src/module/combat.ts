import type { DocumentModificationOptions } from '@league-of-foundry-developers/foundry-vtt-types/src/foundry/common/abstract/document.mjs'
import type { CombatantDataProperties } from '@league-of-foundry-developers/foundry-vtt-types/src/foundry/common/data/data.mjs/combatantData'
import type { BaseUser } from '@league-of-foundry-developers/foundry-vtt-types/src/foundry/common/documents.mjs'
import type { PropertiesToSource } from '@league-of-foundry-developers/foundry-vtt-types/src/types/helperTypes'
import { Character } from './character/character.js'
import type { InitiativeFormula } from './ruleset/rules/basic-combat.js'
import { getGame } from './utils.js'

declare global {
  interface DocumentClassConfig {
    Combatant: typeof DsaCombatant
  }

  interface FlagConfig {
    Combatant: {
      'dsa-41': {
        lastInitiative: number | null
      }
    }
  }
}

export class DsaCombatant extends Combatant {
  private initiativeFormula: InitiativeFormula | undefined
  initiativeValue: number | null

  async _preUpdate(
    changed: DeepPartial<PropertiesToSource<CombatantDataProperties>>,
    options: DocumentModificationOptions,
    user: BaseUser
  ): Promise<void> {
    this.updateInitiativeFormula()

    const initiative = hasProperty(changed, 'initiative')
      ? this.computeInitiative(changed.initiative || null)
      : this.initiative

    setProperty(changed, 'flags.dsa-41.lastInitiative', initiative)

    await super._preUpdate(changed, options, user)
  }

  _getInitiativeFormula(): string {
    this.updateInitiativeFormula()
    return this.initiativeFormula?.rollFormula || ''
  }

  private computeInitiative(modifier: number | null): number | null {
    if (this.initiativeFormula === undefined) {
      this.updateInitiativeFormula()
    }

    let result = modifier
      ? this.initiativeFormula?.initiative(modifier) || null
      : null

    if (result === null) {
      result = this.getFlag('dsa-41', 'lastInitiative') || null
    }

    return result
  }

  protected _initialize(): void {
    super._initialize()
    this.initiativeValue = this._source.initiative
    Object.defineProperty(this, 'initiative', {
      get: () => {
        const modifier = Number.isNumeric(this.initiativeValue)
          ? Number(this.initiativeValue)
          : null

        return this.computeInitiative(modifier)
      },
      // eslint-disable-next-line @typescript-eslint/no-empty-function
      set() {},
      configurable: true,
    })
  }

  updateInitiativeFormula(): void {
    if (this.actor) {
      this.initiativeFormula = new Character(this.actor).initiative()
    }
  }
}

const initiativeRelevantData = [
  'system.base.combatAttributes.active.baseInitiative.value',
  'system.base.combatState',
] as const

function updateCombatantInitiative(actor: Actor): void {
  if (actor.token !== null && actor.token !== undefined) {
    const combatant = actor.token.combatant as DsaCombatant
    if (combatant !== null) {
      combatant.updateInitiativeFormula()
      if (combatant.isOwner) {
        combatant.setFlag('dsa-41', 'lastInitiative', combatant.initiative)
      }
      // token.combatant.updateInitiativeFormula()
      // const combat = token.combatant.parent
      // combat.setupTurns()
      // combat.collection.render()
    }
  }
}

Hooks.on(
  'updateActiveEffect',
  (
    effect: ActiveEffect,
    _change: DeepPartial<ActiveEffectData>,
    _options: any,
    userId: string
  ): void => {
    const changeKeys = effect.changes.map((change) => change.key)

    const game = getGame()
    if (
      userId === game.user?.id &&
      initiativeRelevantData.some((dataEntry: string) =>
        changeKeys.includes(dataEntry)
      )
    ) {
      updateCombatantInitiative(effect.parent)
    }
  }
)

Hooks.on(
  'updateActor',
  (
    actor: Actor,
    change: DeepPartial<ActorData>,
    _options: any,
    userId: string
  ): void => {
    const game = getGame()
    if (
      userId === game.user?.id &&
      initiativeRelevantData.some((dataEntry: string) =>
        hasProperty(change, dataEntry)
      )
    ) {
      updateCombatantInitiative(actor)
    }
  }
)
