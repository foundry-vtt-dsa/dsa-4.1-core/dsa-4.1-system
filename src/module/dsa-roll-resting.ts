import {
  BasicRollActionResult,
  SpeakerProvider,
} from './ruleset/rules/basic-roll-mechanic.js'
import { Effect } from './ruleset/rule-components.js'
import { SkillActionResult } from './ruleset/rules/basic-skill.js'
import { EffectIdentifier } from './model/ruleset.js'
import {
  RestingActionResult,
  RestingActionResultEntry,
} from './ruleset/rules/basic-resting-action.js'
import { getLocalizer } from './utils.js'
import { AttributeListData } from '../../dist/src/module/model/character-data.js'

interface ResourceChatBonusRollEntry {
  dieRoll: number
  modifier: string
}
interface ResourceChatBonusEntry {
  name: string
  value: number
  roll: ResourceChatBonusRollEntry
}

interface ResourceChatEntry {
  sum: string
  dieRoll: number
  formula: string
  bonus: ResourceChatBonusEntry
}

interface ResourceChatData {
  characterName: string | null
  vitality: ResourceChatEntry
  astralEnergy?: ResourceChatEntry
}

function getResourceData(
  characterAttributes: AttributeListData<string>,
  resourceResult: RestingActionResultEntry
): ResourceChatEntry {
  function formatRollFormula(
    roll: BasicRollActionResult | undefined,
    modifier: number
  ) {
    let formula = '1d6'
    if (!roll) {
      formula = modifier >= 0 ? `+${modifier}` : `${modifier}`
    } else if (modifier !== 0) {
      formula += modifier > 0 ? ` +${modifier}` : `${modifier}`
    }
    return formula
  }

  function formatBonusModifierString(bonusModifier: number): string {
    return bonusModifier > 0 ? `+${bonusModifier}` : `${bonusModifier}`
  }

  function formatSumString(sum: number) {
    return sum > 0 ? `+${sum}` : '-'
  }
  function getDieRollResult(roll: BasicRollActionResult | undefined) {
    return roll?.roll.terms[0].results[0].result
  }

  function getBonusRollResult(resourceResult: RestingActionResultEntry) {
    return resourceResult.bonusRollResult?.roll.terms[0].results[0].result
  }

  const roll: BasicRollActionResult | undefined = resourceResult.rollResult
  const modifier: number = resourceResult.options.modifier

  return {
    sum: formatSumString(resourceResult.sum),
    dieRoll: getDieRollResult(roll),
    formula: formatRollFormula(roll, modifier),
    bonus: {
      name: getLocalizer()(resourceResult.options.bonusAttribute),
      value: characterAttributes[resourceResult.options.bonusAttribute].value,
      roll: {
        dieRoll: getBonusRollResult(resourceResult),
        modifier: formatBonusModifierString(
          resourceResult.options.bonusModifier
        ),
      },
    },
  }
}

export class RestRollChatEffect extends Effect<RestingActionResult> {
  private speakerProvider: SpeakerProvider

  constructor(
    identifier: EffectIdentifier<SkillActionResult>,
    speakerProvider: SpeakerProvider
  ) {
    super(identifier)
    this.speakerProvider = speakerProvider
  }

  async _apply<R extends RestingActionResult>(result: R): Promise<any> {
    const characterAttributes =
      result.options.character.data.system.base.basicAttributes

    const resultData: ResourceChatData = {
      characterName: result.options.character.data.name,
      vitality: getResourceData(characterAttributes, result.vitality),
    }

    if (result.astralEnergy) {
      resultData.astralEnergy = getResourceData(
        characterAttributes,
        result.astralEnergy
      )
    }

    const chatData = {
      speaker: this.speakerProvider.getSpeaker({
        actor: result.options.character.data,
      }),
      content: await renderTemplate(
        'systems/dsa-41/templates/chat/resting-roll.html',
        resultData
      ),
    }

    ChatMessage.create(chatData)

    return chatData
  }
}
