import type { Ruleset } from '../ruleset.js'
import type { BaseComputationOptionData } from '../../model/ruleset.js'
import type { CombatTalent } from '../../model/properties.js'
import { DescribeRule } from '../rule.js'
import type { ModifierDescriptor } from '../../model/modifier.js'
import {
  CombatComputationData,
  CombatComputationResult,
  RangedCombatComputationData,
  CombatDamageComputationData,
  DamageFormula,
} from '../../model/rules/derived-combat-attributes.js'
import { CreateComputationIdentifier } from '../rule-components.js'

function ExtractTalentFromWeapon<OptionData extends CombatComputationData>(
  options: OptionData
) {
  if (options.talent) return options
  if (options.weapon) {
    options.talent = options.character.talent(
      options.weapon.talent
    ) as CombatTalent
    return options
  }
  return options
}

export function IsUnarmed(options: CombatComputationData): boolean {
  return (
    options.weapon === undefined &&
    options.talent !== undefined &&
    options.talent.isUnarmed
  )
}

export function IsArmed(options: CombatComputationData): boolean {
  return !IsUnarmed(options)
}

interface DamageFormulaOptions {
  multiplier?: number
  bonusDamage?: number
}

export function createDamageFormula(
  baseDamage: string,
  options: DamageFormulaOptions = {}
): DamageFormula {
  return {
    baseDamage,
    multiplier: options.multiplier || 1,
    bonusDamage: options.bonusDamage || 0,
    get formula() {
      let damageFormula = this.baseDamage
      if (this.multiplier > 1) {
        damageFormula = `${this.multiplier}*(${damageFormula})`
      }
      if (this.bonusDamage !== 0) {
        damageFormula = `${damageFormula} + ${this.bonusDamage}`
      }
      return damageFormula
    },
  }
}

export const ComputeAttack = CreateComputationIdentifier<
  CombatComputationData,
  CombatComputationResult
>('attack')

export const ComputeParry = CreateComputationIdentifier<
  CombatComputationData,
  CombatComputationResult
>('parry')

export const ComputeRangedAttack = CreateComputationIdentifier<
  RangedCombatComputationData,
  CombatComputationResult
>('rangedAttack')

export const ComputeDamageFormula = CreateComputationIdentifier<
  CombatDamageComputationData,
  DamageFormula
>('damageFormula')

export const ComputeDodge = CreateComputationIdentifier<
  CombatComputationData,
  CombatComputationResult
>('dodge')

export const ComputeArmorClass = CreateComputationIdentifier<
  BaseComputationOptionData,
  number
>('armorClass')

export const ComputeEncumbarance = CreateComputationIdentifier<
  BaseComputationOptionData,
  number
>('encumbarance')

interface EffectiveEncumbaranceOptionData extends BaseComputationOptionData {
  formula: string
}

export const ComputeEffectiveEncumbarance = CreateComputationIdentifier<
  EffectiveEncumbaranceOptionData,
  number
>('effectiveEncumbarance')

function addManeuversToModifiers(
  options: CombatComputationData,
  result: CombatComputationResult
): CombatComputationResult {
  if (options.maneuvers !== undefined) {
    options.maneuvers.forEach((maneuver) => {
      result.modifiers?.set(maneuver.name, maneuver)
    })
  }
  return result
}

function ComputeCombatValues(type: 'attack' | 'parry' | 'rangedAttack') {
  const [baseValue, talentMod] = {
    attack: ['baseAttack', 'attackMod'] as const,
    parry: ['baseParry', 'parryMod'] as const,
    rangedAttack: ['baseRangedAttack', 'rangedAttackMod'] as const,
  }[type]
  return (options: CombatComputationData): CombatComputationResult => {
    const value = options.talent
      ? options.character[baseValue] + options.talent[talentMod]
      : 0
    const modifiers = options.modifiers || new Map<string, ModifierDescriptor>()
    return {
      value,
      mod: 0,
      modifiers,
    }
  }
}

export const DerivedCombatAttributesRule = DescribeRule(
  'derived-combat-attributes-rule',
  {
    changeable: false,
    enabled: true,
  },
  (ruleset: Ruleset): void => {
    const computeActions = [
      ComputeAttack,
      ComputeParry,
      ComputeRangedAttack,
    ] as const
    computeActions.forEach((action) => {
      const name = action.name as 'attack' | 'parry' | 'rangedAttack'
      ruleset.on(action).do(ComputeCombatValues(name))
      ruleset.before(action).do(ExtractTalentFromWeapon)
      ruleset.after(action).do(addManeuversToModifiers)
    })

    ruleset.on(ComputeDodge).do(
      (options: CombatComputationData): CombatComputationResult => ({
        value: options.character.baseDodge,
        mod: 0,
        modifiers: options.modifiers || new Map<string, ModifierDescriptor>(),
      })
    )

    ruleset
      .on(ComputeDamageFormula)
      .do((options: CombatDamageComputationData): DamageFormula => {
        return createDamageFormula(options.weapon?.damage || '0', {
          bonusDamage: options.bonusDamage,
          multiplier: options.damageMultiplier,
        })
      })

    ruleset.after(ComputeDamageFormula).do((options, result) => {
      if (options.character.data.bonusDamage !== undefined) {
        result.bonusDamage += options.character.data.bonusDamage
      }
      return result
    })

    ruleset
      .after(ComputeDamageFormula)
      .when(IsUnarmed)
      .do((_options, result) => {
        result.baseDamage = '1d6'
        return result
      })

    ruleset
      .on(ComputeEncumbarance)
      .do((options: BaseComputationOptionData): number => {
        return options.character.armorItems
          ? options.character.armorItems.reduce(
              (prev, current) => current.encumbarance + prev,
              0
            )
          : 0
      })

    ruleset
      .on(ComputeEffectiveEncumbarance)
      .do((options: EffectiveEncumbaranceOptionData): number => {
        if (options.formula === undefined || options.formula === null) {
          return 0
        }
        let encumbarance = options.character.encumbarance
        const formula = options.formula?.replace('–', '-')
        const operators = ['+', '-', 'x']
        const formulaParts =
          formula?.split(
            new RegExp('([' + operators.map((op) => '\\' + op).join('') + '])+')
          ) || []
        if (formulaParts?.length === 3) {
          switch (formulaParts[1]) {
            case '+':
              encumbarance = encumbarance + parseInt(formulaParts[2])
              break
            case '-':
              encumbarance = Math.max(
                encumbarance - parseInt(formulaParts[2]),
                0
              )
              break
            case 'x':
              encumbarance = encumbarance * parseInt(formulaParts[2])
              break
          }
        }
        return encumbarance
      })

    ruleset
      .on(ComputeArmorClass)
      .do((options: BaseComputationOptionData): number => {
        return options.character.armorItems
          ? options.character.armorItems.reduce(
              (prev, current) => current.armorClass + prev,
              0
            )
          : 0
      })
  }
)
