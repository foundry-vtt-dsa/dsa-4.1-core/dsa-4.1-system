import { DescribeRule } from '../rule.js'
import { Ruleset } from '../ruleset.js'
import { ComputeRangedAttack } from './derived-combat-attributes.js'
import {
  RangedAttackAction,
  RangedCombatActionData,
} from './basic-ranged-combat.js'
import { RangedWeapon, Weapon } from '../../model/items.js'
import { ModifierDescriptor, ModifierTable } from '../../model/modifier.js'
import type { SpecialAbility } from '../../model/properties.js'
import { totalModifier } from './basic-roll-mechanic.js'
import { genericI18nMap } from '../../i18n.js'
import {
  generateModificationFromBoolean,
  generateModificationFromMultiplier,
  generateModificationFromTable,
} from './modification-generator.js'
import {
  CoverModifierName,
  CoverModifierNames,
  DarknessModifierName,
  DarknessModifierNames,
  MovementModifierName,
  MovementModifierNames,
  RangedAttackDurationComputationData,
  SightModifierName,
  SightModifierNames,
  SteepshotModifierName,
  SteepshotModifierNames,
  WindModifierName,
  WindModifierNames,
} from '../../model/rules/enhanced-ranged-combat.js'
import { type CombatComputationResult } from '../../model/rules/derived-combat-attributes.js'
import { CreateComputationIdentifier } from '../rule-components.js'
import { AttackActionResult } from './basic-combat.js'

const DarknessModifiers: ModifierTable<DarknessModifierName> = {
  daylight: 0,
  dawn: 2,
  moonlight: 4,
  starlight: 6,
  dark: 8,
}

export const DarknessI18nMap = genericI18nMap(DarknessModifierNames)

const SightModifiers: ModifierTable<SightModifierName> = {
  normal: 0,
  dust: 2,
  fog: 4,
  invisibleTarget: 8,
}

export const SightI18nMap = genericI18nMap(SightModifierNames)

const CoverModifiers: ModifierTable<CoverModifierName> = {
  none: 0,
  half: 2,
  threeQuarter: 4,
}

export const CoverI18nMap = genericI18nMap(CoverModifierNames)

const MovementModifiers: ModifierTable<MovementModifierName> = {
  immobile: -4,
  stationary: -2,
  slowMovement: 0,
  fastMovement: 2,
  evasive: 4,
}

export const MovementI18nMap = genericI18nMap(MovementModifierNames)

const WindModifiers: ModifierTable<WindModifierName> = {
  windstill: 0,
  crosswind: 4,
  strongCrosswind: 8,
}
export const WindI18nMap = genericI18nMap(WindModifierNames)

const SteepshotModifiers: ModifierTable<SteepshotModifierName> = {
  none: 0,
  downwarts: 2,
  upwarts: 4,
}
export const SteepshotI18nMap = genericI18nMap(SteepshotModifierNames)

const DarknessModification = generateModificationFromTable(
  DarknessModifiers,
  'darkness'
)

const SightModification = generateModificationFromTable(SightModifiers, 'sight')

const CoverModification = generateModificationFromTable(CoverModifiers, 'cover')
const MovementModification = generateModificationFromTable(
  MovementModifiers,
  'movement'
)
const WindModification = generateModificationFromTable(WindModifiers, 'wind')
const SteepshotModification = generateModificationFromTable(
  SteepshotModifiers,
  'steepshot'
)

const QuickshotModification = generateModificationFromBoolean('quickshot', 2)
const SecondShotModification = generateModificationFromBoolean(
  'secondAttackInRound',
  4
)

const AimTimeModification = generateModificationFromMultiplier(
  'aimTime',
  -0.5,
  -4
)

const FrayModification = generateModificationFromMultiplier('fray', 2)

export const ComputeRangedAttackDuration = CreateComputationIdentifier<
  RangedAttackDurationComputationData,
  CombatComputationResult
>('rangedAttackDuration')

function isRangedWeapon(weapon: Weapon): weapon is RangedWeapon {
  return weapon.type === 'ranged'
}

function getRangedAttackDuration(
  options: RangedAttackDurationComputationData
): CombatComputationResult {
  const modifiers = new Map<string, ModifierDescriptor>()
  if (options.weapon && isRangedWeapon(options.weapon)) {
    const prepareTime = totalModifier(options.prepareTimeModifiers)
    const loadtime = Math.max(
      0,
      options.weapon.loadtime + totalModifier(options.loadtimeModifiers)
    )
    const shootTime = Math.max(
      0,
      totalModifier(options.shootDurationModifiers) + 1
    )
    return {
      value: Math.max(1, prepareTime + loadtime + shootTime),
      modifiers,
    }
  }
  return { value: 0, modifiers }
}

export const Sharpshooter: SpecialAbility = {
  name: 'Scharfschütze',
  identifier: 'ability-scharfschuetze',
}

export const Marksman: SpecialAbility = {
  name: 'Meisterschütze',
  identifier: 'ability-meisterschuetze',
}

export const Quickload: SpecialAbility = {
  name: 'Schnellladen',
  identifier: 'ability-schnellladen',
}

function QuickloadModifierAdder(forceQuickload = false) {
  return (
    options: RangedAttackDurationComputationData
  ): RangedAttackDurationComputationData => {
    if (forceQuickload || options.character.has(Quickload)) {
      if (options.weapon && isRangedWeapon(options.weapon)) {
        let quickloadMod = 0
        if (options.weapon.talent === 'talent-bogen') {
          quickloadMod = -1
        }
        if (
          ['talent-armbrust', 'talent-bela'].includes(options.weapon.talent)
        ) {
          quickloadMod = -0.25 * options.weapon.loadtime
        }
        options.loadtimeModifiers = [
          ...(options.loadtimeModifiers || []),
          {
            name: 'quickload',
            mod: quickloadMod,
            modifierType: 'other',
          },
        ]
      }
    }
    return options
  }
}

function AddArrowOnBowLoadtimeModifier(
  options: RangedAttackDurationComputationData
): RangedAttackDurationComputationData {
  if (options.isArrowOnBow) {
    options.loadtimeModifiers = [
      ...(options.loadtimeModifiers || []),
      {
        name: 'arrowOnBow',
        mod: -1,
        modifierType: 'other',
      },
    ]
  }
  return options
}

function AddAimTimeShootDurationModifier(
  options: RangedAttackDurationComputationData
): RangedAttackDurationComputationData {
  if (options.aimTime) {
    options.shootDurationModifiers = [
      ...(options.shootDurationModifiers || []),
      {
        name: 'aimTime',
        mod: options.aimTime,
        modifierType: 'other',
      },
    ]
  }
  return options
}

function ApplyRangeBonusDamage(
  options: RangedCombatActionData,
  result: AttackActionResult<RangedCombatActionData>
) {
  if (
    options.rangeClass &&
    options.weapon &&
    isRangedWeapon(options.weapon) &&
    result.damage
  ) {
    result.damage.bonusDamage = options.weapon.bonusDamages[options.rangeClass]
  }
  return result
}

function ModifyAttackDurationForStetchedBow(
  options: RangedAttackDurationComputationData,
  result: CombatComputationResult
) {
  if (
    options.isBowStretched &&
    options.weapon &&
    isRangedWeapon(options.weapon)
  ) {
    result.value = Math.max(
      1,
      result.value -
        (totalModifier(options.loadtimeModifiers) + options.weapon.loadtime)
    )
  }
  return result
}

function AddQuickshotShootDurationModifier(
  options: RangedAttackDurationComputationData
) {
  if (options.quickshot) {
    options.shootDurationModifiers = [
      ...(options.shootDurationModifiers || []),
      {
        name: 'quickshot',
        mod: -1,
        modifierType: 'other',
      },
    ]
  }
  return options
}

function AddAxxeleratusDurationModifiers(
  options: RangedAttackDurationComputationData
) {
  if (options.usesAxxeleratus) {
    if (!options.character.has(Quickload)) {
      options = QuickloadModifierAdder(true)(options)
    } else {
      options.loadtimeModifiers = [
        ...(options.loadtimeModifiers || []),
        {
          name: 'axxeleratus',
          mod: -1,
          modifierType: 'other',
        },
      ]
    }
  }
  return options
}

function AddShootDurationModiferForCustomModifier(
  options: RangedAttackDurationComputationData
) {
  if (options.customModifier) {
    let aimDurationMod = Math.ceil(options.customModifier / 2)
    if (options.character.has(Sharpshooter)) {
      aimDurationMod = Math.max(1, aimDurationMod - 2)
    }
    if (options.character.has(Marksman)) {
      aimDurationMod = 1
    }
    options.shootDurationModifiers = [
      ...(options.shootDurationModifiers || []),
      {
        name: 'custom',
        mod: aimDurationMod,
        modifierType: 'other',
      },
    ]
  }
  return options
}

export const EnhancedRangedCombatRule = DescribeRule(
  'enhanced-ranged-combat-rule',
  {
    changeable: true,
    enabled: true,
  },
  (ruleset: Ruleset): void => {
    const modifications = [
      DarknessModification,
      SightModification,
      CoverModification,
      MovementModification,
      WindModification,
      SteepshotModification,
      QuickshotModification,
      SecondShotModification,
      AimTimeModification,
      FrayModification,
    ]
    modifications.forEach((modification) => {
      ruleset.after(ComputeRangedAttack).do(modification)
      ruleset.before(RangedAttackAction).do(modification)
    })

    ruleset.after(RangedAttackAction).do(ApplyRangeBonusDamage)

    ruleset.on(ComputeRangedAttackDuration).do(getRangedAttackDuration)
    ruleset
      .before(ComputeRangedAttackDuration)
      .do(AddAimTimeShootDurationModifier)

    ruleset
      .before(ComputeRangedAttackDuration)
      .do(AddQuickshotShootDurationModifier)

    ruleset
      .before(ComputeRangedAttackDuration)
      .do(AddArrowOnBowLoadtimeModifier)

    ruleset
      .before(ComputeRangedAttackDuration)
      .do(AddAxxeleratusDurationModifiers)

    ruleset
      .after(ComputeRangedAttackDuration)
      .do(ModifyAttackDurationForStetchedBow)

    ruleset.before(ComputeRangedAttackDuration).do(QuickloadModifierAdder())

    ruleset
      .before(ComputeRangedAttackDuration)
      .do(AddShootDurationModiferForCustomModifier)
  }
)
