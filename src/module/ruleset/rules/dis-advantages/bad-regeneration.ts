import type { BaseProperty } from '../../../model/properties.js'
import { DescribeRule } from '../../rule.js'
import { Ruleset } from '../../ruleset.js'
import { ComputeVitalityRestingModifier } from '../basic-resting-computation.js'
import { CharacterHas } from '../maneuvers/basic-maneuver.js'

export const BadRegeneration: BaseProperty = {
  name: 'Schlechte Regeneration',
  identifier: 'disadvantage-schlechte-regeneration',
}
export const BadRegenerationRule = DescribeRule(
  'bad-regeneration',
  {
    changeable: true,
    enabled: true,
  },
  (ruleset: Ruleset): void => {
    ruleset
      .after(ComputeVitalityRestingModifier)
      .when(CharacterHas(BadRegeneration))
      .do((options, result) => {
        const regenerationBonus = options.character.data.disadvantage(
          'disadvantage-schlechte-regeneration'
        )?.system.value
        result.modifier -= regenerationBonus || 1
        result.bonusModifier += regenerationBonus || 2
        return result
      })
  }
)
