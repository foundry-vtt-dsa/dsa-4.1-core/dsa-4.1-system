import { GlasscannonRule } from './glasscannon.js'
import { UnshakeableRule } from './unshakeable.js'
import { BadRegenerationRule } from './bad-regeneration.js'

export const DisAdvantageRules = [
  BadRegenerationRule,
  GlasscannonRule,
  UnshakeableRule,
]
