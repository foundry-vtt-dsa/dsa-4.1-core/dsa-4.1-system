import type { BaseCharacter } from '../../model/character.js'
import type { SkillDescriptor, TestAttributes } from '../../model/properties.js'
import { DescribeRule } from '../rule.js'
import { Action, CreateActionIdentifier } from '../rule-components.js'
import {
  BaseActionOptionData,
  BaseActionResultType,
} from '../../model/ruleset.js'
import type { Ruleset } from '../ruleset.js'
import {
  Messagable,
  Rollable,
  RollSkill,
  RollSkillToChatEffect,
  SkillRollActionData,
  totalModifier,
} from './basic-roll-mechanic.js'
import { ModifierDescriptor } from '../../model/modifier.js'

export interface SkillActionData extends BaseActionOptionData {
  skill: SkillDescriptor
  character: BaseCharacter
  modifiers?: Map<string, ModifierDescriptor>
  testAttributes?: TestAttributes
}

export interface SkillActionResult
  extends BaseActionResultType<SkillActionData & SkillRollActionData> {
  mod: number
  roll: Rollable & Messagable
  success: boolean
  damage?: string
  critical: boolean
}

export const TalentAction = CreateActionIdentifier<
  SkillActionData,
  SkillActionResult
>('talent')
export const SpellAction = CreateActionIdentifier<
  SkillActionData,
  SkillActionResult
>('spell')
export const SourceAction = CreateActionIdentifier<
  SkillActionData,
  SkillActionResult
>('source')
export const FormulaAction = CreateActionIdentifier<
  SkillActionData,
  SkillActionResult
>('formula')
export const LiturgyAction = CreateActionIdentifier<
  SkillActionData,
  SkillActionResult
>('liturgy')

export class SkillAction extends Action<SkillActionData, SkillActionResult> {
  async _execute<OptionData extends SkillActionData>(
    options: OptionData
  ): Promise<SkillActionResult> {
    let skill
    switch (options.skill.skillType) {
      case 'talent':
        skill = options.character.talent(options.skill.identifier)
        break
      case 'spell':
        skill = options.character.spell(options.skill.identifier)
        break
      case 'source':
        skill = options.character.source(options.skill.identifier)
        break
      case 'formula':
        skill = options.character.formula(options.skill.identifier)
        break
      case 'liturgy':
        skill = options.character.liturgy(options.skill.identifier)
        break
    }
    const testAttributes = options.testAttributes
      ? options.testAttributes
      : skill.testAttributes
    const testAttributeData = testAttributes.map((attributeName) => {
      const attribute = options.character.attribute(attributeName)
      return { name: attribute.name, value: attribute.value }
    })
    const rollResultPromise = this.ruleset.execute(RollSkill, {
      ...options,
      skillName: options.skill.name,
      skillType: options.skill.skillType,
      skillValue: skill.value,
      testAttributeData,
    })
    return rollResultPromise.then((rollResult) => ({
      mod: totalModifier(options.modifiers),
      roll: rollResult.roll,
      success: rollResult.success,
      critical: rollResult.critical,
      options: {
        ...options,
        ...rollResult.options,
      },
    }))
  }
}

export const BasicSkillRule = DescribeRule(
  'basic-skill-rule',
  {
    changeable: false,
    enabled: true,
  },
  (ruleset: Ruleset): void => {
    ruleset.on(TalentAction).do(SkillAction)
    ruleset.after(TalentAction).trigger(RollSkillToChatEffect)
    ruleset.on(SpellAction).do(SkillAction)
    ruleset.after(SpellAction).trigger(RollSkillToChatEffect)
    ruleset.on(SourceAction).do(SkillAction)
    ruleset.after(SourceAction).trigger(RollSkillToChatEffect)
    ruleset.on(FormulaAction).do(SkillAction)
    ruleset.after(FormulaAction).trigger(RollSkillToChatEffect)
    ruleset.on(LiturgyAction).do(SkillAction)
    ruleset.after(LiturgyAction).trigger(RollSkillToChatEffect)
  }
)
