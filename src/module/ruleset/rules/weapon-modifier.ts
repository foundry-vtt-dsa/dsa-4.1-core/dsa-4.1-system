import type { MeleeWeapon } from '../../model/items.js'
import { DescribeRule } from '../rule.js'
import type { Ruleset } from '../ruleset.js'
import { usesMeleeWeapon } from './basic-combat.js'
import { ComputeAttack, ComputeParry } from './derived-combat-attributes.js'

export const WeaponModifierRule = DescribeRule(
  'weapon-modifier',
  {
    changeable: true,
    enabled: true,
  },
  (ruleset: Ruleset): void => {
    ruleset
      .after(ComputeAttack)
      .when(usesMeleeWeapon)
      .do((options, result) => ({
        ...result,
        value: result.value + (<MeleeWeapon>options.weapon).weaponMod.attack,
      }))

    ruleset
      .after(ComputeParry)
      .when(usesMeleeWeapon)
      .do((options, result) => {
        if (!options.shield) {
          result.value += (<MeleeWeapon>options.weapon).weaponMod.parry
        }
        return result
      })
  }
)
