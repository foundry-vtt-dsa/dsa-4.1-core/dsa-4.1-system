import type { Ruleset } from '../ruleset.js'
import {
  BaseActionOptionData,
  BaseActionResultType,
} from '../../model/ruleset.js'
import { DescribeRule } from '../rule.js'
import {
  Action,
  CreateActionIdentifier,
  CreateEffectIdentifier,
} from '../rule-components.js'
import { AttributeName } from '../../model/character-data.js'
import { BaseCharacter } from '../../model/character.js'
import {
  AttributeActionResult,
  BasicRoll,
  BasicRollActionResult,
  RollAttributeWithoutChat,
} from './basic-roll-mechanic.js'

export interface ResourceRestingModifiers {
  roll: boolean
  bonusAttribute: AttributeName
  modifier: number
  bonusModifier: number
}

export interface RestingActionParameters {
  vitality: ResourceRestingModifiers
  astralEnergy?: ResourceRestingModifiers
}

export interface RestingActionResultEntry {
  options: ResourceRestingModifiers
  rollResult?: BasicRollActionResult
  bonusRollResult?: AttributeActionResult
  bonusSuccess?: boolean
  sum: number
}

export interface RestingActionData extends BaseActionOptionData {
  character: BaseCharacter
  options: RestingActionParameters
}

export interface RestingActionResult
  extends BaseActionResultType<RestingActionData> {
  vitality?: RestingActionResultEntry
  astralEnergy?: RestingActionResultEntry
}

export const RestingAction = CreateActionIdentifier<
  RestingActionData,
  RestingActionResult
>('resting')

export const RollRestingToChatEffect =
  CreateEffectIdentifier<RestingActionResult>('rollRestingToChat')

export class Rest extends Action<RestingActionData, RestingActionResult> {
  async _execute(options: RestingActionData): Promise<RestingActionResult> {
    const result = {
      options: {
        ...options,
      },
    } as RestingActionResult

    result.vitality = await this.rollResting(options, 'vitality')

    if (options.options.astralEnergy) {
      result.astralEnergy = await this.rollResting(options, 'astralEnergy')
    }

    options.character.applyResting(
      result.vitality.sum,
      result.astralEnergy?.sum
    )

    return result
  }

  async rollResting(
    data: RestingActionData,
    resourceName: string
  ): Promise<RestingActionResultEntry> {
    const resourceOptions = data.options[resourceName]
    const character = data.character

    const result: RestingActionResultEntry = {
      options: resourceOptions,
      sum: 0,
    }

    if (resourceOptions.roll) {
      result.rollResult = await this.ruleset.execute(BasicRoll, {
        character,
        ...resourceOptions,
        formula: `1d6+${resourceOptions.modifier}`,
      })

      result.sum = result.rollResult.roll.total || 0
    } else {
      result.sum = resourceOptions.modifier
    }

    result.bonusRollResult = await this.ruleset.execute(
      RollAttributeWithoutChat,
      {
        character,
        attributeName: resourceOptions.bonusAttribute,
        mod: resourceOptions.bonusModifier,
        targetValue: character.attribute(resourceOptions.bonusAttribute).value,
      }
    )

    result.sum += result.bonusRollResult.success ? 1 : 0

    return result
  }
}

export const BasicRestingActionRule = DescribeRule(
  'resting-action-rule',
  {
    changeable: false,
    enabled: true,
  },
  (ruleset: Ruleset): void => {
    ruleset.on(RestingAction).do(Rest)
    ruleset.after(RestingAction).trigger(RollRestingToChatEffect)
  }
)
