import type { ModifierDescriptor, ModifierTable } from '../../model/modifier.js'
import { PostHook, PreHook } from '../../model/ruleset.js'
import { totalModifier } from './basic-roll-mechanic.js'

interface ModifierData {
  modifiers: Map<string, ModifierDescriptor>
  mod: number
}

interface ModifierCalculator<
  OptionData extends ModifierData = ModifierData,
  OptionValue = any,
> {
  (options: OptionData, value?: OptionValue): number
}

interface ModifierPredicate<
  OptionData extends ModifierData = ModifierData,
  OptionValue = any,
> {
  (options: OptionData, value?: OptionValue): boolean
}

interface ModifierNameCalculator<
  OptionData extends ModifierData = ModifierData,
  OptionValue = any,
> {
  (options: OptionData, value?: OptionValue): string
}

interface ModifierKeyCalculator<OptionValue = any> {
  (value: OptionValue): string
}

interface ModifierMultiplierCalculator<OptionValue = any> {
  (value: OptionValue): number
}

interface ModifierValuesCalculator<
  OptionData extends ModifierData = ModifierData,
  OptionValue = any,
> {
  (options: OptionData): (OptionValue | undefined)[]
}

type Hook<OptionData extends ModifierData = ModifierData, ResultType = any> =
  | PostHook<OptionData, ResultType>
  | PreHook<OptionData>

type RequireOnlyOne<T, Keys extends keyof T = keyof T> = Pick<
  T,
  Exclude<keyof T, Keys>
> &
  {
    [K in Keys]-?: Required<Pick<T, K>> &
      Partial<Record<Exclude<Keys, K>, undefined>>
  }[Keys]

type ModificationGeneratorOptions<
  OptionData extends ModifierData = ModifierData,
  OptionValue = any,
> = RequireOnlyOne<
  {
    modifierCalculator: ModifierCalculator<OptionData, OptionValue>
    modifierPredicate: ModifierPredicate<OptionData, OptionValue>
    modifierName?: string
    modifierNameCalculator?: ModifierNameCalculator<OptionData, OptionValue>
    modifierClass: string
    modifierValuesCalculator?: ModifierValuesCalculator<OptionData, OptionValue>
    modifierKeyCalculator?: ModifierKeyCalculator<OptionValue>
    modifierMultiplierCalculator?: ModifierMultiplierCalculator<OptionValue>
  },
  'modifierName' | 'modifierNameCalculator'
>

function modificationGenerator<
  OptionData extends ModifierData = ModifierData,
  ResultType extends ModifierData = ModifierData,
  OptionValue = any,
>(
  generatorOptions: ModificationGeneratorOptions<OptionData, OptionValue>
): Hook<OptionData, ResultType> {
  function hook(options: OptionData)
  function hook(options: OptionData, result: ResultType)
  function hook(options: OptionData, result?: ResultType) {
    if (options.modifiers === undefined) {
      options.modifiers = new Map<string, ModifierDescriptor>()
    }
    const modifierNameCalculator =
      generatorOptions.modifierName !== undefined
        ? () => generatorOptions.modifierName
        : generatorOptions.modifierNameCalculator
    const values =
      generatorOptions.modifierValuesCalculator !== undefined
        ? generatorOptions.modifierValuesCalculator(options)
        : [undefined]
    values.forEach((value) => {
      if (generatorOptions.modifierPredicate(options, value)) {
        addModifier(generatorOptions, options, modifierNameCalculator, value)
      }
    })
    const totalMod = totalModifier(options.modifiers)
    if (result) {
      result.modifiers = options.modifiers
      return result
    }
    options.mod = totalMod
    return options
  }
  return hook
}

function addModifier<
  OptionData extends ModifierData = ModifierData,
  OptionValue = any,
>(
  generatorOptions: ModificationGeneratorOptions<OptionData, OptionValue>,
  options: OptionData,
  modifierNameCalculator: ModifierNameCalculator<OptionData, OptionValue>,
  value?: OptionValue
) {
  const modifierKey =
    generatorOptions.modifierKeyCalculator !== undefined && value !== undefined
      ? generatorOptions.modifierKeyCalculator(value)
      : generatorOptions.modifierClass
  const modifierMultiplier =
    generatorOptions.modifierMultiplierCalculator !== undefined &&
    value !== undefined
      ? generatorOptions.modifierMultiplierCalculator(value)
      : 1
  if (modifierMultiplier > 0) {
    options.modifiers.set(modifierKey, {
      name: modifierNameCalculator(options, value),
      mod: generatorOptions.modifierCalculator(options, value),
      class: generatorOptions.modifierClass,
      modifierType: 'other',
      multiplier: modifierMultiplier,
    })
  }
}

export function generateModificationFromBoolean<
  K extends string,
  OptionData extends ModifierData & Record<K, boolean | undefined>,
>(optionKey: K, modifier: number) {
  return modificationGenerator<OptionData>({
    modifierCalculator: () => modifier,
    modifierPredicate: (options) => options[optionKey] === true,
    modifierClass: optionKey,
    modifierName: optionKey,
  })
}

export function generateModificationFromMultiplier<
  K extends string,
  OptionData extends ModifierData & Record<K, number>,
>(optionKey: K, baseModifier: number, minCap?: number) {
  return modificationGenerator<OptionData>({
    modifierCalculator: (options) => {
      const multiplier = options[optionKey]

      if (multiplier && typeof multiplier === 'number') {
        let modifier = Math.ceil(baseModifier * multiplier)
        if (minCap) {
          modifier = Math.max(minCap, modifier)
        }
        return modifier
      }
      return 0
    },
    modifierPredicate: (options) => options[optionKey] !== undefined,
    modifierClass: optionKey,
    modifierName: optionKey,
  })
}

export function generateModificationFromTable<
  KeyNames extends string,
  K extends string,
  OptionData extends ModifierData & Record<K, KeyNames>,
>(table: ModifierTable<KeyNames>, optionKey: K) {
  return modificationGenerator<OptionData>({
    modifierCalculator: (options) => table[options[optionKey]],
    modifierPredicate: (options) =>
      options[optionKey] !== undefined &&
      table[options[optionKey]] !== undefined,
    modifierClass: optionKey,
    modifierNameCalculator: (options) => `${options[optionKey]}`,
  })
}

interface ModifierConfig<K extends string> {
  name: K
  count: number
}

export function generateModificationMapFromTable<
  KeyNames extends string,
  K extends string,
  OptionData extends ModifierData &
    Record<K, Map<KeyNames, ModifierConfig<KeyNames>>>,
>(table: ModifierTable<KeyNames>, optionKey: K) {
  return modificationGenerator<
    OptionData,
    ModifierData,
    ModifierConfig<KeyNames>
  >({
    modifierCalculator: (_options, value) =>
      value !== undefined ? table[value.name] : 0,
    modifierPredicate: (options, value) =>
      options[optionKey] !== undefined &&
      value !== undefined &&
      table[value.name] !== undefined,
    modifierClass: optionKey,
    modifierNameCalculator: (_options, value) => value?.name || '',
    modifierValuesCalculator: (options) =>
      options[optionKey] !== undefined ? [...options[optionKey].values()] : [],
    modifierKeyCalculator: (value) => value.name,
    modifierMultiplierCalculator: (value) => value.count,
  })
}
