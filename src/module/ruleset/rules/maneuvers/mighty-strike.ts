import { Maneuver } from '../../../character/maneuver.js'
import type { SpecialAbility } from '../../../model/properties.js'
import { DescribeRule } from '../../rule.js'
import type { Ruleset } from '../../ruleset.js'
import { ComputeDamageFormula } from '../derived-combat-attributes.js'
import type {
  CombatDamageComputationData,
  DamageFormula,
} from '../derived-combat-attributes.js'
import {
  AddManeuver,
  ComputeManeuverList,
  IsArmedManeuver,
  IsOffensiveManeuver,
} from './basic-maneuver.js'

export const MightyStrike: SpecialAbility = {
  name: 'Wuchtschlag',
  identifier: 'ability-wuchtschlag',
}

export const MightyStrikeManeuver = new Maneuver('mightyStrike', 'offensive')

export const MightyStrikeRule = DescribeRule(
  'mightyStrike',
  {
    changeable: true,
    enabled: true,
  },
  (ruleset: Ruleset): void => {
    ruleset
      .after(ComputeDamageFormula)
      .do((options: CombatDamageComputationData, result: DamageFormula) => {
        const mightyStrike = options.modifiers?.get(MightyStrikeManeuver.name)
        if (mightyStrike) {
          if (options.character.has(MightyStrike)) {
            result.bonusDamage += mightyStrike.mod
          } else {
            result.bonusDamage += Math.ceil(mightyStrike.mod / 2)
          }
        }

        return result
      })

    ruleset
      .after(ComputeManeuverList)
      .when(IsOffensiveManeuver)
      .when(IsArmedManeuver)
      .do(AddManeuver(MightyStrikeManeuver))
  }
)
