import { Maneuver } from '../../../character/maneuver.js'
import type { SpecialAbility } from '../../../model/properties.js'

import { createManeuverRule } from './maneuver-creator.js'

export const TargetedStab: SpecialAbility = {
  name: 'Gezielter Stich',
  identifier: 'ability-gezielter-stich',
}

export const TargetedStabManeuver = new Maneuver('targetedStab', 'offensive', {
  minMod: 4,
})
export const TargetedStabRule = createManeuverRule(
  TargetedStab,
  TargetedStabManeuver
)
