import { CreateComputationIdentifier } from '../../rule-components.js'

import { DescribeRule } from '../../rule.js'
import { Ruleset } from '../../ruleset.js'
import {
  FormulaCastTimeClass,
  FormulaDurationClass,
  FormulaDurationClasses,
  FormulaParameterClassNames,
  FormulaParameters,
  FormulaRangeClass,
  FormulaStructureClass,
  FormulaTargetClass,
  FormulaWeightClasses,
  SphereType,
} from '../../../model/myranor-magic.js'
import { ModifierDescriptor, ModifierTable } from '../../../model/modifier.js'
import { BaseComputationOptionData } from '../../../model/ruleset.js'
import {
  FormulaData,
  FormulaInstructionCost,
  FormulaInstructionData,
} from '../../../model/item-data.js'
import { Advantage, Formula } from '../../../model/properties.js'

const FormulaCastTimeModifiers: ModifierTable<FormulaCastTimeClass> = {
  oneHour: -1,
  ritual: 0,
  sixGameRounds: 0,
  oneGameRounds: 1,
  twentyActions: 2,
  tenActions: 3,
  fiveActions: 4,
  threeActions: 5,
  twoActions: 6,
  oneAction: 7,
}

const FormulaRangeModifiers: ModifierTable<FormulaRangeClass> = {
  self: -1,
  touch: 0,
  oneStep: 1,
  threeSteps: 2,
  sevenSteps: 3,
  twentyOneSteps: 4,
  fortyNineSteps: 5,
  horizon: 6,
  outOfSight: 7,
}

const FormulaTargetModifiers: ModifierTable<FormulaTargetClass> = {
  oneCreature: -1,
  oneObject: 0,
  spellValueCreatures: 0,
  oneStepZone: 1,
  spellValueObjects: 2,
  threeTimesSpellValueCreatures: 2,
  spellValueStepsZone: 3,
  threeTimesSpellValueObjects: 3,
  threeTimesSpellValueStepsZone: 4,
  anyNumberOfCreatures: 5,
  anyNumberOfObject: 6,
  anyZone: 7,
}

const FormulaDurationModifiers: ModifierTable<FormulaDurationClass> = {
  leftSpellPointsActions: -1,
  fiftyActions: 0,
  oneGameRound: 1,
  leftSpellPointsGameRounds: 1,
  instantaneousNatural: 1,
  leftSpellPointsHours: 2,
  leftSpellPointsTimes8hours: 3,
  oneWeek: 4,
  oneMonth: 5,
  oneYear: 6,
  instantaneousPermanent: 7,
}

const FormulaStructureModifiers: ModifierTable<FormulaStructureClass> = {
  extremeEasy: -1,
  veryEasy: 0,
  easy: 1,
  difficult: 2,
  veryDifficult: 3,
  extremeDifficult: 4,
  complex: 5,
  veryComplex: 6,
  extremeComplex: 7,
}

export const FormulaModifications = {
  castTime: FormulaCastTimeModifiers,
  target: FormulaTargetModifiers,
  range: FormulaRangeModifiers,
  duration: FormulaDurationModifiers,
  structure: FormulaStructureModifiers,
} as const

interface MyranorFormulaModifierOptionData extends BaseComputationOptionData {
  formula: Formula
  modifiers: Map<string, ModifierDescriptor>
}

export const ComputeMyranorFormulaModifier = CreateComputationIdentifier<
  MyranorFormulaModifierOptionData,
  Map<string, ModifierDescriptor>
>('myranorFormulaMod')

export const ComputeMyranorSourceModifier = CreateComputationIdentifier<
  MyranorFormulaModifierOptionData,
  Map<string, ModifierDescriptor>
>('myranorSourceMod')

interface AuraConjunctionEntry {
  name: string
  identifier: string
}

export const AuraConjunctionMap: Record<SphereType, AuraConjunctionEntry> = {
  demonic: {
    name: 'Aurakonjunktion (Dämonisch)',
    identifier: 'advantage-auraconjunction-demonic',
  },
  elemental: {
    name: 'Aurakonjunktion (Elementare)',
    identifier: 'advantage-auraconjunction-elemental',
  },
  death: {
    name: 'Aurakonjunktion (Totenwesen)',
    identifier: 'advantage-auraconjunction-death',
  },
  nature: {
    name: 'Aurakonjunktion (Naturwesen)',
    identifier: 'advantage-auraconjunction-nature',
  },
  stellar: {
    name: 'Aurakonjunktion (Stellare)',
    identifier: 'advantage-auraconjunction-stellar',
  },
  time: {
    name: 'Aurakonjunktion (Zeit)',
    identifier: 'advantage-auraconjunction-time',
  },
}

export function GetModifiersTotal(
  formulaParameters: FormulaParameters
): number {
  return FormulaParameterClassNames.map(
    (parameterClass) =>
      FormulaModifications[parameterClass][formulaParameters[parameterClass]]
  ).reduce((a, b) => a + b)
}

export function GetAspCosts(
  formula: FormulaData,
  instructions: FormulaInstructionData[]
) {
  function getCostParameters(
    formula: FormulaData,
    instructions: FormulaInstructionData[]
  ) {
    const instruction = instructions.find((i) => i.id === formula.instructionId)
    const costs = instruction?.system.costs
    const structure = formula.parameters.structure
    const costsParameters = costs && (costs[structure] || costs.generic)
    return costsParameters
  }

  function calculateWeightCosts(
    formula: FormulaData,
    costsParameters: FormulaInstructionCost
  ): number {
    if (!costsParameters.weight || !costsParameters.weightBase) {
      return 0
    }
    const weightPower =
      FormulaWeightClasses.indexOf(formula.weight) -
      FormulaWeightClasses.indexOf(costsParameters.weightBase)
    return Math.max(1, costsParameters.weight * 2 ** weightPower)
  }

  function calculateDurationCosts(
    formula: FormulaData,
    costsParameters: FormulaInstructionCost
  ): number {
    if (!costsParameters.duration || !costsParameters.durationBase) {
      return 0
    }

    const durationSteps: FormulaDurationClass[] = FormulaDurationClasses.filter(
      (duration) => duration !== 'instantaneousNatural'
    )

    const durationPower =
      durationSteps.indexOf(formula.parameters.duration) -
      durationSteps.indexOf(costsParameters?.durationBase)
    return Math.max(1, costsParameters.duration * 2 ** durationPower)
  }

  const costsParameters = getCostParameters(formula, instructions)

  if (!costsParameters) {
    return
  }

  const durationCosts = calculateDurationCosts(formula, costsParameters)
  const weightCosts = calculateWeightCosts(formula, costsParameters)
  const additionalCosts = formula.additionalAsp || 0
  const total = Math.round(
    (costsParameters.base || 0) + durationCosts + weightCosts + additionalCosts
  )

  return {
    total: total,
    base: costsParameters.base || 0,
    duration: durationCosts,
    weight: weightCosts,
    additional: additionalCosts,
    remarks: costsParameters.remarks,
  }
}

export function CharacterHasConjunctionToSphere(
  advantageSphere: SphereType,
  advantage: Advantage
): (options: MyranorFormulaModifierOptionData) => boolean {
  return (options: MyranorFormulaModifierOptionData): boolean => {
    const value =
      options.character.data.advantage(advantage?.identifier)?.system.value || 0
    if (value < 2) {
      return false
    }

    const formula = options.formula || options.skill
    // spontaneous casting uses the source + sphere directly VS fixed formulas refer to a source
    const formulaSphere = formula.source?.sphere

    return formulaSphere === advantageSphere
  }
}

export const BasicMyranorFormulaRule = DescribeRule(
  'myranor-formula-modifier-rule',
  {
    changeable: false,
    enabled: true,
  },
  (ruleset: Ruleset): void => {
    const calculateMods = (options: MyranorFormulaModifierOptionData) => {
      const formulaItem = options.formula.item
      const formulaParameterModValues = FormulaParameterClassNames.map(
        (parameterClass) => {
          const parameterValue = formulaItem.system.parameters[parameterClass]
          return FormulaModifications[parameterClass][parameterValue]
        }
      )

      const formulaParameterModifier = formulaParameterModValues.reduce(
        (a, b) => a + b,
        0
      )

      options.modifiers.set('formulaParameterModifier', {
        name: 'formulaParameterModifier',
        mod: formulaParameterModifier,
        modifierType: 'other',
      })

      options.modifiers.set('formulaQuality', {
        name: 'formulaQuality',
        mod: -1 * options.formula.quality,
        modifierType: 'other',
      })

      return options.modifiers
    }

    ruleset.on(ComputeMyranorFormulaModifier).do(calculateMods)

    for (const [advantageSphere, advantage] of Object.entries(
      AuraConjunctionMap
    )) {
      ruleset
        .before(ComputeMyranorFormulaModifier)
        .when(CharacterHasConjunctionToSphere(advantageSphere, advantage))
        .do((options: MyranorFormulaModifierOptionData) => {
          options.modifiers.set('auraConjunction', {
            name: 'auraConjunction',
            mod: -3,
            modifierType: 'other',
          })

          return options
        })
    }
  }
)

export const BasicMyranorSourceRule = DescribeRule(
  'myranor-source-modifier-rule',
  {
    changeable: false,
    enabled: true,
  },
  (ruleset: Ruleset): void => {
    const calculateMods = (options: MyranorFormulaModifierOptionData) => {
      const formula: Formula = options.formula

      const modifiers = new Map<string, ModifierDescriptor>()

      FormulaParameterClassNames.forEach((category) => {
        modifiers.set(category, {
          name: `formula-${category}`,
          mod: 0,
          modifierType: 'formulaModifier',
          hidden: true,
        })
      })

      const effectiveEncumbarance = options.character.effectiveEncumbarance(
        formula.effectiveEncumbarance.formula
      )

      modifiers.set('effectiveEncumbarance', {
        name: 'effectiveEncumbarance',
        mod: effectiveEncumbarance,
        modifierType: 'other',
      })

      const sphere = formula.source?.sphere
      const characterHasConjunctionToSphere = CharacterHasConjunctionToSphere(
        sphere,
        AuraConjunctionMap[sphere]
      )({
        character: options.character,
        formula,
      })

      if (characterHasConjunctionToSphere) {
        modifiers.set('auraConjunction', {
          name: 'auraConjunction',
          mod: -3,
          modifierType: 'other',
        })
      }

      return modifiers
    }

    ruleset.on(ComputeMyranorSourceModifier).do(calculateMods)
  }
)
