import type { Ruleset } from '../ruleset.js'
import { BaseComputationOptionData } from '../../model/ruleset.js'
import { DescribeRule } from '../rule.js'
import { CreateComputationIdentifier } from '../rule-components.js'
import { AttributeName } from '../../model/character-data.js'
import { RestingActionParameters } from './basic-resting-action.js'
import { BaseCharacter } from '../../model/character.js'

export interface RestingOptions {
  comfort: number
  vitalityMod: number
  astralEnergyMod: number
  weather: number
  restInterrupted: boolean
  keptWatch: boolean
  badEncampment: boolean
}

export interface RestingOptionsComputationData
  extends BaseComputationOptionData {
  options: RestingOptions
}

export interface ResourceRestingModifiers {
  roll: boolean
  bonusAttribute: AttributeName
  modifier: number
  bonusModifier: number
}

interface RestingModifierComputationOptionDataEntry
  extends BaseComputationOptionData {
  modifiers: ResourceRestingModifiers
}

export const ComputeAstralRestingModifier = CreateComputationIdentifier<
  RestingModifierComputationOptionDataEntry,
  ResourceRestingModifiers
>('astralRestingMod')

export const ComputeVitalityRestingModifier = CreateComputationIdentifier<
  RestingModifierComputationOptionDataEntry,
  ResourceRestingModifiers
>('vitalityRestingMod')

export const ComputeRestingParameters = CreateComputationIdentifier<
  RestingOptionsComputationData,
  RestingActionParameters
>('resting-parameters')

function calculateResourceParameters(
  character: BaseCharacter,
  attributeName: AttributeName,
  options: RestingOptions,
  modName: 'vitalityMod' | 'astralEnergyMod',
  modFunctionName: 'vitalityRestingModifier' | 'astralEnergyRestingModifier'
): ResourceRestingModifiers {
  const mod: number =
    options[modName] +
    options.comfort +
    options.weather +
    (options.restInterrupted ? -1 : 0) +
    (options.keptWatch ? -1 : 0) +
    (options.badEncampment ? -1 : 0)

  return character[modFunctionName]({
    roll: true,
    bonusAttribute: attributeName,
    modifier: mod,
    bonusModifier: 0,
  })
}

function calculateVitalityParameters(
  options: RestingOptionsComputationData
): ResourceRestingModifiers {
  return calculateResourceParameters(
    options.character,
    'constitution',
    options.options,
    'vitalityMod',
    'vitalityRestingModifier'
  )
}

function calculateAstralEnergyParameters(
  options: RestingOptionsComputationData
): ResourceRestingModifiers {
  return calculateResourceParameters(
    options.character,
    'intuition',
    options.options,
    'astralEnergyMod',
    'astralEnergyRestingModifier'
  )
}

function isBelowMax(resource) {
  return resource.value < resource.max
}

function CalculateRestingParameters(
  options: RestingOptionsComputationData
): RestingActionParameters {
  const parameters = {} as RestingActionParameters

  if (isBelowMax(options.character.data.system.base.resources.vitality)) {
    parameters.vitality = calculateVitalityParameters(options)
  }

  if (
    isBelowMax(options.character.data.system.base.resources.astralEnergy) &&
    options.character.data.system.settings.hasAstralEnergy
  ) {
    parameters.astralEnergy = calculateAstralEnergyParameters(options)
  }

  return parameters
}

const CalculateResourceParameters = (
  options: RestingModifierComputationOptionDataEntry
): ResourceRestingModifiers => options.modifiers

export const BasicRestingComputationRule = DescribeRule(
  'basic-resting-computation-rule',
  {
    changeable: false,
    enabled: true,
  },
  (ruleset: Ruleset): void => {
    ruleset.on(ComputeRestingParameters).do(CalculateRestingParameters)
    ruleset.on(ComputeAstralRestingModifier).do(CalculateResourceParameters)
    ruleset.on(ComputeVitalityRestingModifier).do(CalculateResourceParameters)
  }
)
