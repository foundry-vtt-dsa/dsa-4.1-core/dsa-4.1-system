import { DsaItemSheet } from './item-sheet.js'
import { FormulaStructureClasses } from '../model/myranor-magic.js'

export class FormulaInstructionSheet extends DsaItemSheet {
  activateListeners(html) {
    super.activateListeners(html)
  }

  async getData() {
    const data = await super.getData()

    data.formulaStructureClasses = FormulaStructureClasses

    return data
  }
}
