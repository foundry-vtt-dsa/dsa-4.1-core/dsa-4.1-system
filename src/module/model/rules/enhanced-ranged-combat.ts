import { ModifierDescriptor } from '../modifier.js'
import {
  CombatComputationData,
  RangedCombatOptionData,
} from './derived-combat-attributes.js'

declare module './derived-combat-attributes.js' {
  interface RangedCombatOptionData {
    darkness: DarknessModifierName
    sight: SightModifierName
    cover: CoverModifierName
    movement: MovementModifierName
    wind: WindModifierName
    steepshot: SteepshotModifierName
    quickshot: boolean
    secondAttackInRound: boolean
    aimTime: number
    fray: number
    customModifier: number
    isArrowOnBow: boolean
    isBowStretched: boolean
    usesAxxeleratus: boolean
  }
}

export const DarknessModifierNames = [
  'daylight',
  'dawn',
  'moonlight',
  'starlight',
  'dark',
] as const

export type DarknessModifierName = (typeof DarknessModifierNames)[number]

export const SightModifierNames = [
  'normal',
  'dust',
  'fog',
  'invisibleTarget',
] as const
export type SightModifierName = (typeof SightModifierNames)[number]

export const CoverModifierNames = ['none', 'half', 'threeQuarter'] as const
export type CoverModifierName = (typeof CoverModifierNames)[number]

export const MovementModifierNames = [
  'immobile',
  'stationary',
  'slowMovement',
  'fastMovement',
  'evasive',
] as const
export type MovementModifierName = (typeof MovementModifierNames)[number]

export const WindModifierNames = [
  'windstill',
  'crosswind',
  'strongCrosswind',
] as const
export type WindModifierName = (typeof WindModifierNames)[number]

export const SteepshotModifierNames = ['none', 'upwarts', 'downwarts'] as const
export type SteepshotModifierName = (typeof SteepshotModifierNames)[number]

export interface RangedAttackDurationComputationData
  extends CombatComputationData,
    RangedCombatOptionData {
  loadtimeModifiers?: ModifierDescriptor[]
  shootDurationModifiers?: ModifierDescriptor[]
  prepareTimeModifiers?: ModifierDescriptor[]
}
