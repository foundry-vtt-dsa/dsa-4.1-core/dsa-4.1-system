export const attributeNames = [
  'courage',
  'cleverness',
  'intuition',
  'charisma',
  'agility',
  'dexterity',
  'constitution',
  'strength',
] as const

export type AttributeName = (typeof attributeNames)[number]

export interface AttributeData {
  value: number
}

export type AttributeListData<AttrName extends string> = Record<
  AttrName,
  AttributeData
>

export const resourceNames = [
  'vitality',
  'endurance',
  'karmicEnergy',
  'astralEnergy',
] as const

export type ResourceName = (typeof resourceNames)[number]

export interface ResourceData {
  value: number
  min: number
  max: number
}

export type ResourceListData<ResName extends string> = Record<
  ResName,
  ResourceData
>

export const activeCombatAttributeNames = [
  'baseInitiative',
  'baseAttack',
  'baseParry',
  'baseRangedAttack',
  'dodge',
]

export type ActiveCombatAttributeName =
  (typeof activeCombatAttributeNames)[number]

export const passiveCombatAttributeNames = [
  'magicResistance',
  'physicalResistance',
]

export type PassiveCombatAttributeName =
  (typeof passiveCombatAttributeNames)[number]

export interface WoundThresholds {
  first: number
  second: number
  third: number
}

export interface WoundThresholdsWithMod extends WoundThresholds {
  mod: number
}

interface CombatAttributesData {
  active: AttributeListData<ActiveCombatAttributeName>
  passive: AttributeListData<PassiveCombatAttributeName> & {
    woundThresholds: WoundThresholdsWithMod
  }
}

interface SpeedData {
  value: number
  unit: 'Schritt'
}

interface MovementData {
  speed: SpeedData
}

type ItemId = string | undefined

interface CombatStateData {
  isArmed: boolean
  primaryHand: ItemId
  secondaryHand: ItemId
  unarmedTalent: ItemId
}

interface SocialStatusData {
  value: number
}

interface CharacterSocialData {
  socialStatus: SocialStatusData
  titles: string[]
  nobility: string | null
}

interface CharacterAppearanceData {
  height: string
  weight: string
  birthday: string
  eyeColor: string
  hairColor: string
}

interface CharacterBackgroundData {
  folk: string | null
  culture: string | null
  profession: string | null
  social: CharacterSocialData
  description: string
}

export interface CharacterBaseData {
  basicAttributes: AttributeListData<AttributeName>
  resources: ResourceListData<ResourceName>
  combatAttributes: CombatAttributesData
  movement: MovementData
  combatState: CombatStateData
}

export interface CharacterSettings {
  autoCalcBaseAttack: boolean
  autoCalcBaseParry: boolean
  autoCalcBaseRangedAttack: boolean
  autoCalcInitiative: boolean
  autoCalcMagicResistance: boolean
  autoCalcWoundThresholds: boolean
  hasAstralEnergy: boolean
  hasKarmicEnergy: boolean
}

type ManeuverModifier = {
  name: string
  value: number | string
  source: string
}

export interface CharacterModifiers {
  maneuverModifiers: ManeuverModifier[]
  bonusDamage: number
}

export interface CharacterData {
  base: CharacterBaseData
  background: CharacterBackgroundData
  appearance: CharacterAppearanceData
  settings: CharacterSettings
  modifiers: CharacterModifiers
}
