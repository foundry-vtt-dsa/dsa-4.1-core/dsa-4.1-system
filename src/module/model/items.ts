import type { ModifierTable } from './modifier.js'

export interface WeaponMod {
  attack: number
  parry: number
}

export interface StrengthMod {
  threshold: number
  hitPointStep: number
}

export type RollFormula = string

export type WeaponType = 'melee' | 'ranged'

export type ItemClass = 'weapon' | 'shield'

export interface EquipableItem {
  class: ItemClass
}

export interface Weapon extends EquipableItem {
  class: 'weapon'
  type: WeaponType
  name: string
  talent: string
  damage: RollFormula
}

export interface MeleeWeapon extends Weapon {
  type: 'melee'
  initiativeMod: number
  weaponMod: WeaponMod
  strengthMod: StrengthMod
}

export const RangeClasses = [
  'veryClose',
  'close',
  'medium',
  'far',
  'veryFar',
] as const
export type RangeClass = (typeof RangeClasses)[number]

export const SizeClasses = [
  'tiny',
  'verySmall',
  'small',
  'medium',
  'big',
  'veryBig',
] as const
export type SizeClass = (typeof SizeClasses)[number]

export interface RangedWeapon extends Weapon {
  type: 'ranged'
  loadtime: number
  bonusDamages: ModifierTable<RangeClass>
}

export interface Shield extends EquipableItem {
  class: 'shield'
  name: string
  initiativeMod: number
  weaponMod: WeaponMod
}

export interface Armor {
  name: string
  armorClass: number
  encumbarance: number
}
