import { SpellVariantData, TimeData } from './item-data.js'
import { ModifierDescriptor } from './modifier.js'

export const SpellModificationsByCategory = {
  general: ['changeTechnique', 'changeCentralTechnique'] as const,
  conditional: ['unvoluntarily', 'voluntarily'] as const,
  castTime: ['halfCastTime', 'doubleCastTime'] as const,
  forceEffect: ['forceEffect'] as const,
  cost: ['reduceCost'] as const,
  multipleTargets: ['multipleCompanions', 'multipleEnemies'] as const,
  range: ['increaseRange', 'reduceRange'] as const,
  duration: [
    'doubleDuration',
    'halfDuration',
    'changeToFixedDuration',
  ] as const,
} as const

export type SpellModificationCategory =
  keyof typeof SpellModificationsByCategory

const getKeys = Object.keys as <T extends object>(
  obj: T
) => readonly (keyof T)[]
export const SpellModificationCategories = getKeys(SpellModificationsByCategory)

export const SpellModfications = Object.values(
  SpellModificationsByCategory
).flat()

export type SpellModification = (typeof SpellModfications)[number]

export const SpellTargetClasses = [
  'object',
  'multipleObjects',
  'person',
  'multiplePersons',
  'zone',
  'creature',
  'multipleCreatures',
  'spell',
  'voluntarily',
] as const

export type SpellTargetClass = (typeof SpellTargetClasses)[number]

type CastTime = TimeData & {
  additionalCastTime: TimeData
}

type ModifiableRange = {
  value: string
  stepModifier: number
}

type ModifiableEffectTime = {
  value: string
  multiplier: number
}

export interface SpellComputationResult {
  modifiers: Map<string, ModifierDescriptor>
  targetClasses: SpellTargetClass[]
  castTime: CastTime
  magicResistance: number | 'max'
  astralCost: string
  range: ModifiableRange
  effectTime: ModifiableEffectTime
  requiresUphold: boolean
}

export interface SpellOptions {
  spontaneousModifications: SpellModification[]
  targetClasses: SpellTargetClass[]
  spellVariants: SpellVariantData[]
}
