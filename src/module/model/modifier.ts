export type ModifierType =
  | 'maneuver'
  | 'maneuverModifier'
  | 'formulaModifier'
  | 'other'

export interface ModifierDescriptor {
  hidden?: boolean
  class?: string
  name: string
  mod: number
  modifierType: ModifierType
  source?: string
  multiplier?: number
  nameIsLocalized?: boolean
}

export type ModifierTable<KeyNames extends string, Value = number> = Record<
  KeyNames,
  Value
>

export type ManeuverType = 'offensive' | 'defensive'

export interface ManeuverDescriptor extends ModifierDescriptor {
  type: ManeuverType
  minMod: number
}

export type ManeuverModifier = {
  name: string
  value: number
  source: string
}
