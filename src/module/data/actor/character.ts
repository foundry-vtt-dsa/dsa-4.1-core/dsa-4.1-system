import { CommonActorData } from './common.js'

const fields = foundry.data.fields

const CharacterDataModelSchema = () => {
  return {
    ...CommonActorData.defineSchema(),
    background: new fields.SchemaField({
      folk: new fields.StringField({ initial: '', required: true }),
      culture: new fields.StringField({ initial: '', required: true }),
      profession: new fields.StringField({ initial: '', required: true }),
      social: new fields.SchemaField({
        socialStatus: new fields.SchemaField({
          value: new fields.NumberField({
            initial: 0,
            integer: true,
            required: true,
            nullable: false,
          }),
        }),
        titles: new fields.ArrayField(
          new fields.StringField({ initial: '', required: true })
        ),
        nobility: new fields.StringField({ initial: '', required: true }),
      }),
      birthday: new fields.StringField({ initial: '', required: true }),
      description: new fields.StringField({ initial: '', required: true }),
    }),
    appearance: new fields.SchemaField({
      height: new fields.StringField({ initial: '', required: true }),
      weight: new fields.StringField({ initial: '', required: true }),
      eyeColor: new fields.StringField({ initial: '', required: true }),
      hairColor: new fields.StringField({ initial: '', required: true }),
    }),
  }
}

export class CharacterDataModel extends CommonActorData<
  ReturnType<typeof CharacterDataModelSchema>
> {
  static defineSchema() {
    return CharacterDataModelSchema()
  }

  static migrateData(source: object): object {
    if (
      'background' in source &&
      typeof source.background === 'object' &&
      'appearance' in source &&
      typeof source.appearance === 'object' &&
      source.appearance !== null &&
      'description' in source.appearance
    ) {
      source.background = {
        ...source.background,
        description: source.appearance?.description,
      }
    }
    return super.migrateData(source)
  }
}
