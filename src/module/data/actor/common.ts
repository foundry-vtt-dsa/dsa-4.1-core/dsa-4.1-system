import {
  activeCombatAttributeNames,
  attributeNames,
  passiveCombatAttributeNames,
  resourceNames,
} from '../../model/character-data.js'
import { itemDataModels } from '../data-models.js'

const fields = foundry.data.fields

function makeAttributeField() {
  return new fields.SchemaField({
    value: new fields.NumberField({
      initial: 0,
      integer: true,
      nullable: false,
      required: true,
    }),
  })
}

function makeResourceField() {
  return new fields.SchemaField({
    value: new fields.NumberField({
      initial: 0,
      integer: true,
      nullable: false,
      required: true,
    }),
    max: new fields.NumberField({
      initial: 0,
      integer: true,
      nullable: false,
      required: true,
    }),
    min: new fields.NumberField({
      initial: 0,
      integer: true,
      nullable: false,
      required: true,
    }),
  })
}

export class CommonActorData<
  Schema extends ReturnType<
    (typeof CommonActorData)['defineSchema']
  > = ReturnType<(typeof CommonActorData)['defineSchema']>,
> extends foundry.abstract.TypeDataModel<Schema, Actor> {
  static defineSchema() {
    return {
      base: new fields.SchemaField({
        basicAttributes: new fields.SchemaField(
          attributeNames.reduce(
            (prev, cur) => ({ ...prev, [cur]: makeAttributeField() }),
            {}
          ) as Record<
            (typeof attributeNames)[number],
            ReturnType<typeof makeAttributeField>
          >
        ),
        resources: new fields.SchemaField(
          resourceNames.reduce(
            (prev, cur) => ({ ...prev, [cur]: makeResourceField() }),
            {}
          ) as Record<
            (typeof resourceNames)[number],
            ReturnType<typeof makeResourceField>
          >
        ),
        combatAttributes: new fields.SchemaField({
          active: new fields.SchemaField(
            activeCombatAttributeNames.reduce(
              (prev, cur) => ({ ...prev, [cur]: makeAttributeField() }),
              {}
            ) as Record<
              (typeof activeCombatAttributeNames)[number],
              ReturnType<typeof makeAttributeField>
            >
          ),
          passive: new fields.SchemaField({
            ...(passiveCombatAttributeNames.reduce(
              (prev, cur) => ({ ...prev, [cur]: makeAttributeField() }),
              {}
            ) as Record<
              (typeof passiveCombatAttributeNames)[number],
              ReturnType<typeof makeAttributeField>
            >),
            woundThresholds: new fields.SchemaField({
              first: new fields.NumberField({
                initial: 0,
                integer: true,
                nullable: false,
                required: true,
              }),
              second: new fields.NumberField({
                initial: 0,
                integer: true,
                nullable: false,
                required: true,
              }),
              third: new fields.NumberField({
                initial: 0,
                integer: true,
                nullable: false,
                required: true,
              }),
              mod: new fields.NumberField({
                initial: 0,
                integer: true,
                nullable: false,
                required: true,
              }),
            }),
          }),
        }),
        movement: new fields.SchemaField({
          speed: new fields.SchemaField({
            value: new fields.NumberField({
              initial: 0,
              integer: true,
              nullable: false,
              required: true,
            }),
            unit: new fields.StringField({
              initial: 'Schritt',
              nullable: false,
              required: true,
              readonly: true,
            }),
          }),
        }),
        combatState: new fields.SchemaField({
          isArmed: new fields.BooleanField({ initial: false, required: true }),
          primaryHand: new fields.StringField({
            initial: undefined,
          }),
          secondaryHand: new fields.StringField({
            initial: undefined,
          }),
          unarmedTalent: new fields.StringField({
            initial: undefined,
          }),
        }),
      }),
      settings: new fields.SchemaField({
        autoCalcBaseAttack: new fields.BooleanField({ initial: true }),
        autoCalcBaseParry: new fields.BooleanField({ initial: true }),
        autoCalcBaseRangedAttack: new fields.BooleanField({ initial: true }),
        autoCalcInitiative: new fields.BooleanField({ initial: true }),
        autoCalcMagicResistance: new fields.BooleanField({ initial: true }),
        autoCalcWoundThresholds: new fields.BooleanField({ initial: true }),
        hasAstralEnergy: new fields.BooleanField({ initial: false }),
        hasKarmicEnergy: new fields.BooleanField({ initial: false }),
      }),
      modifiers: new fields.SchemaField({
        maneuverModifiers: new fields.ArrayField(
          new fields.SchemaField({
            name: new fields.StringField({ initial: '', required: true }),
            value: new fields.NumberField({
              initial: 0,
              required: true,
              nullable: false,
            }),
            source: new fields.StringField({ initial: '', required: true }),
          })
        ),
        bonusDamage: new fields.NumberField({
          initial: 0,
          required: true,
          nullable: false,
        }),
        additionalItems: new fields.ArrayField(
          new fields.SchemaField({
            uuid: new fields.StringField({ initial: '', required: true }),
            name: new fields.StringField({ initial: '', required: true }),
            value: new fields.NumberField({ initial: 0, nullable: false }),
            system: new fields.ObjectField({
              initial: {},
              required: true,
            }) as unknown as foundry.data.fields.DataField<
              ExtractFields<
                InstanceType<
                  (typeof itemDataModels)[keyof typeof itemDataModels]
                >['schema']
              >
            >,
          })
        ),
        bonusDamage: new fields.NumberField({ initial: 0 }),
      }),
    }
  }

  static migrateData(source: Record<string, any>): object {
    const combatStateProps = [
      'isArmed',
      'primaryHand',
      'secondaryHand',
      'unarmedTalent',
    ]

    combatStateProps.forEach((prop) => {
      if (
        source?.base?.combatState !== undefined &&
        prop in source.base.combatState &&
        source.base.combatState[prop] === null
      ) {
        source.base.combatState[prop] = undefined
      }
    })
    return super.migrateData(source)
  }
}

type ExtractFields<T> = T extends foundry.data.fields.SchemaField<infer F>
  ? F
  : never
