import { AttributeName, attributeNames } from '../../model/character-data.js'
import { EffectiveEncumbaranceTypes, timeUnits } from '../../model/item-data.js'

const fields = foundry.data.fields

export const baseItem = () => ({
  description: new fields.HTMLField({ initial: '', required: true }),
})

export const uniquelyIdentifiable = () => ({
  sid: new fields.StringField({ initial: '', required: true }),
  isUniquelyOwnable: new fields.BooleanField({ initial: true }),
})

const makeTestAttributeField = () => {
  return new fields.StringField({
    initial: attributeNames[0],
    choices: attributeNames.reduce(
      (acc, curr) => ({ ...acc, [curr]: curr }),
      {}
    ) as Record<AttributeName, AttributeName>,
    required: true,
  })
}

export function makeChoiceStringField<
  ChoiceType extends string,
  Required extends boolean = true,
>(choices: readonly ChoiceType[], required: Required = true as Required) {
  return new fields.StringField({
    initial: choices[0],
    choices: choices.reduce(
      (acc, curr) => ({ ...acc, [curr]: curr }),
      {}
    ) as Record<ChoiceType, ChoiceType>,
    required,
  })
}

export const testable = () => ({
  test: new fields.SchemaField({
    firstAttribute: makeTestAttributeField(),
    secondAttribute: makeTestAttributeField(),
    thirdAttribute: makeTestAttributeField(),
  }),
})

export const baseTalent = () => ({
  type: new fields.StringField({ initial: '', required: true }),
  category: new fields.StringField({ initial: '', required: true }),
  effectiveEncumbarance: new fields.SchemaField({
    type: makeChoiceStringField(EffectiveEncumbaranceTypes),
    formula: new fields.StringField({ initial: '', required: true }),
  }),
  value: new fields.NumberField({
    initial: 0,
    integer: true,
    required: true,
    nullable: false,
  }),
})

export const baseWeapon = () => ({
  talent: new fields.StringField({ initial: '', required: true }),
  damage: new fields.StringField({ initial: '', required: true }),
})

export const buyable = () => ({
  price: new fields.StringField({ initial: '', required: true }),
})

export const physical = () => ({
  weight: new fields.StringField({ initial: '', required: true }),
})

export const equipable = () => ({
  equipped: new fields.BooleanField({ initial: false }),
})

export const migrateTimeUnits = (source: object) => {
  const timeUnitMap = {
    Aktion: 'action',
    Akt: 'action',
    Runden: 'round',
    Spielrunden: 'round',
    SR: 'round',
    SP: 'round',
    Minuten: 'minute',
    Stunden: 'hour',
    Tage: 'day',
    Wochen: 'week',
    Monate: 'month',
    Jahre: 'year',
  } as const
  if (
    'castTime' in source &&
    typeof source.castTime == 'object' &&
    source.castTime !== null &&
    'unit' in source.castTime &&
    typeof source.castTime.unit == 'string' &&
    !(<ReadonlyArray<string>>timeUnits).includes(source.castTime.unit)
  ) {
    if (source.castTime.unit in timeUnitMap) {
      source.castTime.unit =
        timeUnitMap[source.castTime.unit as keyof typeof timeUnitMap]
    } else {
      source.castTime.unit = 'special'
    }
  }
  return source
}

export const castable = () => ({
  castTime: new fields.SchemaField({
    duration: new fields.NumberField({
      initial: 0,
      integer: true,
      nullable: false,
      required: true,
    }),
    unit: makeChoiceStringField(timeUnits),
    info: new fields.StringField({ initial: '', required: true }),
  }),
  effectTime: new fields.StringField({ initial: '', required: true }),
  targetClasses: new fields.ArrayField(
    new fields.StringField({
      initial: '',
      required: true,
    })
  ),
  range: new fields.StringField({ initial: '', required: true }),
  technique: new fields.StringField({ initial: '', required: true }),
  effect: new fields.StringField({ initial: '', required: true }),
  variants: new fields.ArrayField(
    new fields.StringField({ initial: '', required: true })
  ),
})
