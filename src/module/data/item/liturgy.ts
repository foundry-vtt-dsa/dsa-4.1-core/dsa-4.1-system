import {
  baseItem,
  castable,
  migrateTimeUnits,
  uniquelyIdentifiable,
} from './common.js'

const fields = foundry.data.fields

export class LiturgyDataModel extends foundry.abstract.TypeDataModel<
  ReturnType<(typeof LiturgyDataModel)['defineSchema']>,
  Item.ConfiguredInstance
> {
  static defineSchema() {
    return {
      ...baseItem(),
      ...uniquelyIdentifiable(),
      ...castable(),
      type: new fields.StringField({ initial: '', required: true }),
      degree: new fields.StringField({ initial: '', required: true }),
      castType: new fields.StringField({ initial: '', required: true }),
      llpage: new fields.NumberField({
        initial: null,
        nullable: true,
        required: true,
      }),
      liturgyVariants: new fields.ArrayField(
        new fields.SchemaField({
          degree: new fields.StringField({ initial: '', required: true }),
        })
      ),
    }
  }

  static migrateData(source: object): object {
    source = migrateTimeUnits(source)
    return super.migrateData(source)
  }
}
