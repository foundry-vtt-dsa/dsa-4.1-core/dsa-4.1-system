import { baseItem, buyable, equipable, physical } from './common.js'

const fields = foundry.data.fields

export class ArmorDataModel extends foundry.abstract.TypeDataModel<
  ReturnType<(typeof ArmorDataModel)['defineSchema']>,
  Item.ConfiguredInstance
> {
  static defineSchema() {
    return {
      ...baseItem(),
      ...buyable(),
      ...physical(),
      ...equipable(),
      armorClass: new fields.NumberField({
        initial: 0,
        integer: true,
        required: true,
        nullable: false,
      }),
      encumbarance: new fields.NumberField({
        initial: 0,
        integer: true,
        required: true,
        nullable: false,
      }),
    }
  }
}
