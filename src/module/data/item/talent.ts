import {
  baseItem,
  baseTalent,
  testable,
  uniquelyIdentifiable,
} from './common.js'

const fields = foundry.data.fields

export class TalentDataModel extends foundry.abstract.TypeDataModel<
  ReturnType<(typeof TalentDataModel)['defineSchema']>,
  Item.ConfiguredInstance
> {
  static defineSchema() {
    return {
      ...baseItem(),
      ...baseTalent(),
      ...uniquelyIdentifiable(),
      ...testable(),
    }
  }
}

export class LinguisticTalentDataModel extends foundry.abstract.TypeDataModel<
  ReturnType<(typeof LinguisticTalentDataModel)['defineSchema']>,
  Item.ConfiguredInstance
> {
  static defineSchema() {
    return {
      ...baseItem(),
      ...baseTalent(),
      ...uniquelyIdentifiable(),
      ...testable(),
      complexity: new fields.NumberField({
        initial: 0,
        integer: true,
        required: true,
        nullable: false,
      }),
    }
  }
}

export class CombatTalentDataModel extends foundry.abstract.TypeDataModel<
  ReturnType<(typeof CombatTalentDataModel)['defineSchema']>,
  Item.ConfiguredInstance
> {
  static defineSchema() {
    return {
      ...baseItem(),
      ...baseTalent(),
      ...uniquelyIdentifiable(),
      combat: new fields.SchemaField({
        category: new fields.StringField({ initial: '', required: true }),
        attack: new fields.NumberField({
          initial: -1,
          integer: true,
          required: true,
          nullable: false,
        }),
        parry: new fields.NumberField({
          initial: -1,
          integer: true,
          required: true,
          nullable: false,
        }),
        rangedAttack: new fields.NumberField({
          initial: -1,
          integer: true,
          required: true,
          nullable: false,
        }),
      }),
    }
  }
}
