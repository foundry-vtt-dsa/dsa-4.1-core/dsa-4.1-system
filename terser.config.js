export default {
  compress: {
    booleans_as_integers: false,
    passes: 3,
  },

  // mangle: {
  //   toplevel: true,
  // },
  mangle: false,

  ecma: 2020,

  module: true,
}
